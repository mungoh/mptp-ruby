Rails.application.routes.draw do
  root to: "index#index"

  resources :elections, only: %i[index show] do
    resources :results, controller: "election_results",
                        only: [:show],
                        param: :result_type,
                        constraints: { result_type: /(mpp|fpp)/ }

    get "most-past-post/calculations", to: "most_past_post/calculations#index"
    get "most-past-post/party-rankings", to: "most_past_post/calculations#party_rankings"
  end
end
