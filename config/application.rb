require_relative "boot"

require "active_model/railtie"
require "active_record/railtie"
require "action_controller/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MPTP
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.api_only = true
    config.debug_exception_response_format = :api

    # threshold value that determines when proportional seat allocation kicks in
    # 3% is the max value recommended by the council of europe (see https://en.wikipedia.org/wiki/Election_threshold
    # & citation [Resolution 1547 (2007), para. 58](http://assembly.coe.int/nw/xml/XRef/Xref-XML2HTML-en.asp?fileid=17531&lang=en))
    config.mpp_threshold_percent = 3

    config.active_record.schema_format = :sql
  end
end
