# mptp data engine (ruby)
This is the data engine for [Most Past the Post (MPTP)](https://mostpastpost.info). Most Past the Post is a proposal to reform the First Past the Post voting system. It applies a set of calculations to first past the post election results, reassigning some seats to losing candidates in order to achieve proportional representation.

The data engine is built on ruby/rails (api version) and has rake tasks for loading election data from GB general elections and exporting them to json.
It can be run as a standard Rails API application, but since the datasets are effectively static, they can be generated once and served as static files.


## Requirements
- ruby (v2.6)
- postgres (v11)

## Get started
Install the dependencies
```bash
cd mptp-ruby
gem install bundler
bundle install
```

`rails -T` will list the available tasks the most notable are:
- `rails election:load_gb_general_elections` - (cleans &) loads all elections for which data is available (see db/import)
- `rails export:elections[data_dir]` - exports generated election data & calculations to a set of json files. These files are committed directly into the [mptp-ui public/data](https://gitlab.com/mungoh/mptp-ui/tree/master/public/data/gb) folder for use in the frontend.

## Data formats
The election data has been collated from various public sources over time as and when it was found or became available. For that reason the raw data files are a pretty messy in places.
There's a few places in the import routines where cleanup/normalisation is done to improve the quality of the data that's generated/exported.

Some elections were avalailble in json, some only in csv/excel
TODO: consider moving all the raw data somewhere else
