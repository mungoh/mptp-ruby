require "spec_helper"

describe MPTP::Application.routes, type: :routing do
  describe "root" do
    it "should route to index#index" do
      expect(get: "/").to route_to("index#index")
    end
  end

  describe "elections" do
    it "should route to elections#show" do
      expect(get: "/elections/1").to route_to("elections#show", id: "1")
    end
  end

  describe "election results" do
    context "invalid results type" do
      it "should raise a routing error" do
        expect(get: "/elections/1/results/huh").not_to be_routable
      end
    end

    context "fpp" do
      let(:expected_params) do
        {
          controller: "election_results",
          action: "show",
          election_id: "1",
          result_type: "fpp"
        }
      end
      it "should route to election_results#show" do
        expect(get: "/elections/1/results/fpp").to route_to(expected_params)
      end
    end

    context "mpp" do
      let(:expected_params) do
        {
          controller: "election_results",
          action: "show",
          election_id: "1",
          result_type: "mpp"
        }
      end
      it "should route to election_results#show" do
        expect(get: "/elections/1/results/mpp").to route_to(expected_params)
      end
    end
  end
end
