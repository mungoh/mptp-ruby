FactoryBot.define do
  factory :election do
    polling_date { 3.months.ago }
    country { "GB" }
    election_type { 0 }
    slug { nil }

    trait :with_constituencies do
      transient do
        number_of_constituencies { 5 }
        region_name { "Test South West" }
      end

      after :create do |election, evaluator|
        region = Region.find_or_create_by(name: evaluator.region_name)
        create_list(:constituency, evaluator.number_of_constituencies, election: election, region: region)
      end
    end

    trait :with_results do
      after :build do |election|
        election.first_past_post_result = build(:first_past_post_result)
        election.most_past_post_result = build(:most_past_post_result)
      end
    end
  end

  factory :election_result do
    election
    type { nil }
    threshold { nil }

    factory :first_past_post_result, class: FirstPastPostResult do
      type { "FirstPastPostResult" }
    end

    factory :most_past_post_result, class: MostPastPostResult do
      type { "MostPastPostResult" }
      threshold { Rails.application.config.mpp_threshold_percent }
    end
  end

  factory :region do
    sequence :name do
      ["London", "South East", "South West", "Midlands", "North West", "North East", "Scotland", "Wales", "Northern Ireland"].sample
    end
    slug { nil }
  end

  factory :constituency do
    election
    region { nil }
    sequence :name do
      "#{Faker::Address.city} #{%w[North North-West West South-West South South-East East North-East Central].sample}"
    end
    slug { nil }
    size { Random.new.rand(55000..90000) }

    after :build do |constituency|
      if constituency.region.blank?
        region_attributes = attributes_for(:region)
        constituency.region = Region.find_or_create_by(region_attributes)
      end
    end
  end

  factory :candidate do
    sequence :name do
      Faker::Name.name_with_middle
    end
    slug { nil }
    party { nil }
    constituency
    votes { nil }

    after :build do |candidate|
      if candidate.party.blank?
        party_attributes = attributes_for(:party)
        candidate.party = Party.find_or_create_by(party_attributes)
      end
    end
  end

  factory :party do
    sequence :name do
      "#{Faker::Hipster.words(number: 2).join(' ').titleize} Party"
    end
    abbreviation { nil }
    short_name { nil }
    colour { nil }
    slug { nil }

    after(:build) do |party|
      if party.abbreviation.blank?
        abbreviation = party.name.gsub(/[^A-Z]/, "")
        abbreviation << Random.new.rand(0..99).to_s
        party.abbreviation = abbreviation
      end

      party.short_name = party.name.gsub(/ Party/, "") if party.short_name.blank?
    end
  end

  factory :seat do
    candidate
    constituency
    election_result
  end
end
