class ElectionBuilder
  attr_reader :election

  def initialize(options = {})
    year = options.fetch(:year, Date.current.year)
    num_constituencies = options.fetch(:num_constituencies, 650)
    @election = create(:election, :with_constituencies, year: year, number_of_constituencies: num_constituencies)
  end
end
