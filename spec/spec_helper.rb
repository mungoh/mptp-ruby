ENV["RAILS_ENV"] ||= "test"
require "simplecov"
SimpleCov.start "rails" do
  add_filter "vendor"
end

ENV["RAILS_ENV"] ||= "test"
require File.expand_path("../../config/environment", __FILE__)
require "rspec/rails"
require "rubygems"
require "json"
require "csv"
require "pry"
require "digest/md5"

Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

Faker::Config.locale = "en-GB"

RSpec.configure do |config|
  config.order = "random"
  config.color = true
  config.include FactoryBot::Syntax::Methods
  config.mock_with :rspec do |mocks|
    mocks.verify_doubled_constant_names = true
    mocks.verify_partial_doubles = true
  end

  config.infer_spec_type_from_file_location!
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end
