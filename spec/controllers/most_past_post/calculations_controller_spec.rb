require "spec_helper"

RSpec.describe MostPastPost::CalculationsController, type: :controller do
  before do
    load Rails.root.join("db/seeds/gb-general-election-2015-with-calculations.rb")
  end

  let(:election_slug) { "gb-general-election-2015" }
  let(:election) { Election.find_by!(slug: election_slug) }

  describe "GET index" do
    before do
      get :index, params: { election_id: election.id }
    end

    subject { JSON.parse(response.body) }

    it "returns ok" do
      expect(response).to have_http_status :ok
    end

    it "renders an array of calculations" do
      expect(subject).to be_an Array
      subject.each do |calculation|
        expect(calculation.keys).to eq ["order_seq", "calculation_type", "description", "seats"]
      end
    end
  end

  describe "GET party_rankings" do
    before do
      get :party_rankings, params: { election_id: election.id }
    end

    subject { JSON.parse(response.body) }

    it "returns ok" do
      expect(response).to have_http_status :ok
    end

    it "renders the expected party rankings" do
      expect(subject).to be_an Array
      subject.each do |ranking|
        expect(ranking.keys).to eq %w[name abbreviation slug calculation_stats ranked_candidates]
      end
    end
  end
end
