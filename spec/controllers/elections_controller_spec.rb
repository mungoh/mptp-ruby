require "spec_helper"

RSpec.describe ElectionsController, type: :controller do
  let(:election) { create(:election) }

  describe "GET #index" do
    context "with valid response" do
      before do
        get :index
      end

      it "returns ok" do
        expect(response).to have_http_status :ok
      end

      it "renders an array" do
        expect(JSON.parse(response.body)).to be_an Array
      end
    end
  end

  describe "GET #show" do
    let(:params) { { id: election.id } }

    before do
      get :show, params: params
    end

    context "with error request" do
      context "with missing parameter" do
        let(:params) { { id: "" } }

        it "returns bad request" do
          expect(response).to have_http_status :bad_request
        end
      end

      context "with missing election" do
        let(:params) { { id: "slug-not-found" } }

        it "returns not found" do
          expect(response).to have_http_status :not_found
        end
      end

      context "with unexpected error" do
        before do
          allow(controller).to receive(:show).and_raise StandardError, "unexpected"
          get :show, params: params
        end

        it "returns bad request" do
          expect(response).to have_http_status :bad_request
        end
      end
    end

    context "with valid response" do
      it "returns ok" do
        expect(response).to have_http_status :ok
      end

      it "renders a hash" do
        expect(JSON.parse(response.body)).to be_a Hash
      end
    end
  end
end
