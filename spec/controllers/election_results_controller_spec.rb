require "spec_helper"

RSpec.describe ElectionResultsController, type: :controller do
  describe "GET #show" do
    let(:election) do
      create(:election, :with_constituencies, :with_results)
    end
    let(:fpp_result) do
      { details: "fpp results truncated" }.with_indifferent_access
    end
    let(:mpp_result) do
      { details: "mpp results truncated" }.with_indifferent_access
    end

    before do
      allow(election.first_past_post_result).to receive(:to_json).and_return(fpp_result.to_json)
      allow(election.most_past_post_result).to receive(:to_json).and_return(mpp_result.to_json)
      allow(Election).to receive(:find).with(election.id.to_s).and_return(election)
      get :show, params: { election_id: election.id, result_type: result_type }
    end

    context "with fpp" do
      let(:result_type) { "fpp" }

      subject { JSON.parse(response.body) }

      it "returns ok" do
        expect(response).to have_http_status :ok
      end

      it "renders a hash" do
        expect(JSON.parse(response.body)).to eq fpp_result
      end
    end

    context "with mpp" do
      let(:result_type) { "mpp" }

      it "returns ok" do
        expect(response).to have_http_status :ok
      end

      it "renders a hash" do
        expect(JSON.parse(response.body)).to eq mpp_result
      end
    end
  end
end
