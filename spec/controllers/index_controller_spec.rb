require "spec_helper"

RSpec.describe IndexController, type: :controller do
  describe "GET index" do
    let(:expected_response) do
      {
        "object" => "elections",
        "path" => "/elections"
      }
    end

    before do
      get :index
    end

    it "returns ok" do
      expect(response).to have_http_status :ok
    end

    it "renders the exepcted response" do
      expect(JSON.parse(response.body)).to eq expected_response
    end
  end
end
