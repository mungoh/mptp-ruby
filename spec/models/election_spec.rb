require "spec_helper"

RSpec.describe Election, type: :model do
  describe "data model" do
    context "db fields" do
      it { should have_db_column(:id) }
      it { should have_db_column(:polling_date) }
      it { should have_db_column(:country) }
      it { should have_db_column(:election_type) }
      it { should have_db_column(:created_at) }
      it { should have_db_column(:updated_at) }
      it { should have_one(:first_past_post_result) }
      it { should have_many(:constituencies) }
    end

    context "validation" do
      it { should validate_presence_of(:polling_date) }
      it { should validate_presence_of(:country) }
    end
  end

  describe ".parties" do
    let(:election1) { create(:election, :with_constituencies) }
    let(:parties) { create_list(:party, 4) }

    before do
      election1.constituencies.each do |constituency|
        4.times do |n|
          create(:candidate, constituency_id: constituency.id, votes: 10, party: parties[n])
        end
        constituency.save!
      end
    end

    it "returns the expected parties" do
      expect(election1.parties).to eq parties
    end
  end

  describe ".stats" do
    let(:election1) { create(:election, :with_constituencies) }
    let(:parties) { create_list(:party, 4) }
    let(:expected_stats) do
      {
        constituencies: election1.constituencies.size,
        parties: 4,
        candidates: election1.constituencies.size * 4,
        total_votes: {
          electorate: 351638,
          parties: nil,
          total: election1.constituencies.size * 40,
          turnout: 0.05687667430709991
        },
        fpp_results: nil,
        mpp_results: nil
      }
    end

    before do
      election1.constituencies.each do |constituency|
        4.times do |n|
          create(:candidate, constituency_id: constituency.id, votes: 10, party: parties[n])
        end
        constituency.save!
      end
    end

    it "returns the expected stats" do
      expect(election1.stats.keys).to eq expected_stats.keys
    end
  end

  describe ".as_json" do
    let(:election) { create(:election, :with_constituencies, :with_results) }
    let(:expected_keys) { %i[id polling_date year country election_type created_at updated_at stats uri results] }

    before do
      allow(election.first_past_post_result).to receive(:as_summary_json).and_return(details: "truncated")
      allow(election.most_past_post_result).to receive(:wasted_votes).and_return(1)
      allow(election.most_past_post_result).to receive(:as_summary_json).and_return(details: "truncated")
    end

    subject { election.as_json }

    it "returns expected hash" do
      expect(subject).to be_a Hash
      expect(subject.keys).to eq expected_keys
    end
  end

  describe ".uri" do
    let(:election) { create(:election) }

    context "with no format" do
      it "returns agnostic path" do
        expect(election.uri).to eq "/elections/#{election.slug}"
      end
    end

    context "with json format" do
      it "returns agnostic path" do
        expect(election.uri(:json)).to eq "/elections/#{election.slug}.json"
      end
    end
  end

  describe ".total_votes" do
    let(:election1) { create(:election, :with_constituencies, polling_date: 3.months.ago) }
    let(:election2) { create(:election, :with_constituencies, polling_date: 15.months.ago) }
    let(:parties) { create_list(:party, 4) }

    before do
      election1.constituencies.each do |constituency|
        3.times do |n|
          create(:candidate, constituency_id: constituency.id, votes: 10, party: parties[n])
        end
        constituency.save!
      end

      election2.constituencies.each do |constituency|
        3.times do |n|
          create(:candidate, constituency_id: constituency.id, votes: 3, party: parties[n])
        end
        constituency.save!
      end
    end

    it "returns the expected total votes" do
      expect(election1.total_votes[:total]).to eq 150
      expect(election2.total_votes[:total]).to eq 45
    end
  end

  describe "party_votes" do
    context "with no results available" do
      let(:election) { build(:election) }

      it "returns nil" do
        expect(election.party_votes).to be_nil
      end
    end

    context "with results available" do
      let(:election_slug) { "gb-general-election-2015" }
      let(:election_stats) { JSON.parse(File.read(Rails.root.join("spec/fixtures/elections/stats/#{election_slug}.json"))) }

      before do
        load Rails.root.join("db/seeds/gb-general-election-2015-with-calculations.rb")
      end

      subject do
        election = Election.find_by!(slug: election_slug)
        election.party_votes
      end

      it "returns the expected totals" do
        expect(subject).to eq election_stats["total_votes"]["parties"]
      end
    end
  end

  describe ".pending?" do
    context "with past election" do
      subject { build(:election, :with_constituencies, :with_results, polling_date: 1.month.ago).pending? }

      it "returns false" do
        expect(subject).to eq false
      end
    end

    context "with future election" do
      subject { build(:election, polling_date: 1.month.from_now).pending? }

      it "returns true" do
        expect(subject).to eq true
      end
    end
  end

  describe ".clean" do
    let(:election) { create(:election, :with_constituencies, :with_results) }
    let(:fpp_results_spy) { instance_double("FirstPastPostResult", clean: true, destroy: true) }
    let(:mpp_results_spy) { instance_double("MostPastPostResult", clean: true, destroy: true) }
    let(:constituencies_spy) { instance_double("ActiveRecord::Associations::CollectionProxy", delete_all: true) }
    let(:candidate) { create(:candidate, election: election, constituency: election.constituencies.first) }

    it "cleans & destroys the most past post results" do
      allow(election).to receive(:most_past_post_result).and_return(mpp_results_spy)
      election.clean
      expect(mpp_results_spy).to have_received(:clean)
      expect(mpp_results_spy).to have_received(:destroy)
    end

    it "cleans & destroys the first past post results" do
      allow(election).to receive(:first_past_post_result).and_return(fpp_results_spy)
      election.clean
      expect(fpp_results_spy).to have_received(:clean)
      expect(fpp_results_spy).to have_received(:destroy)
    end

    it "deletes all it's constituencies" do
      allow(election).to receive(:constituencies).and_return(constituencies_spy)
      election.clean
      expect(constituencies_spy).to have_received(:delete_all)
    end
  end

  describe ".calculate_mpp_threshold" do
    let(:election) { create(:election, :with_constituencies, :with_results) }

    before do
      allow(election).to receive(:total_votes).and_return(total: 1000)
    end

    it "returns the expected threshold" do
      expect(election.calculate_mpp_threshold).to eq 30.0
    end
  end

  describe ".generate_results" do
    let(:election) { create(:election, :with_constituencies, :with_results) }
    let(:fpp_spy) { instance_double("FirstPastPostResult", generate: true) }
    let(:mpp_generator_spy) { instance_double("MPTP::ResultCalculator", run: true) }

    before do
      allow(FirstPastPostResult).to receive(:find_or_create_by).with(election_id: election.id).and_return(fpp_spy)
      allow(MPTP::ResultCalculator).to receive(:new).with(election).and_return(mpp_generator_spy)
      allow(election).to receive(:save!).and_return(true)
      allow(election).to receive(:reload).and_return(true)
    end

    it "generates first past post results" do
      election.generate_results
      expect(fpp_spy).to have_received(:generate)
    end

    it "generates most past post results" do
      election.generate_results
      expect(mpp_generator_spy).to have_received(:run)
    end

    it "saves and reloads the election" do
      election.generate_results
      expect(election).to have_received(:save!)
      expect(election).to have_received(:reload).twice
    end
  end
end
