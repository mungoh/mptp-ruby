require "spec_helper"

RSpec.describe FirstPastPostResult, type: :model do
  describe ".assign_seat" do
    let(:election) { create(:election, :with_constituencies, :with_results) }
    let(:constituency) { election.constituencies.first }
    let(:candidate) { create(:candidate, constituency: constituency) }

    subject { election.first_past_post_result }

    it "assigns the expected seat" do
      seat = subject.assign_seat(constituency, candidate)
      expect(seat).to be_a Seat
      expect(subject.seats.size).to eq 1
    end
  end

  describe ".generate" do
    before do
      load Rails.root.join("db/seeds/gb-general-election-2015-no-calculations.rb")
      fpp_result.generate
    end

    context "with gb-general-election-2015 results" do
      let(:election_slug) { "gb-general-election-2015" }
      let(:stats_fixture) do
        JSON.parse(File.read(Rails.root.join("spec/fixtures/elections/stats/#{election_slug}.json")))
            .deep_symbolize_keys
      end
      let(:expected_stats) { stats_fixture[:fpp_results] }
      let(:election) { Election.find_by!(slug: election_slug) }
      let(:fpp_result) { described_class.find_or_create_by(election: election) }

      before do
        fpp_result.generate
      end

      subject { fpp_result }

      it "should have generated expected seats & stats" do
        subject.reload
        expect(subject.seats.size).to eq 650
        expect(subject.stats).to eq expected_stats
      end
    end
  end
end
