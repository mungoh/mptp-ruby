require "spec_helper"

RSpec.describe Constituency, type: :model do
  context "db fields" do
    it { should have_db_column(:id) }
    it { should have_db_column(:name) }
    it { should have_db_column(:slug) }
    it { should have_db_column(:size) }
    it { should have_db_column(:created_at) }
    it { should have_db_column(:updated_at) }
    it { should belong_to(:election) }
    it { should belong_to(:region) }
    it { should have_many(:candidates) }
  end

  context "validation" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:slug) }
    #
    # context "uniqueness" do
    #   let(:election) { create(:election) }
    #
    #   subject { create(:constituency, election: election) }
    #
    #   it { should validate_uniqueness_of(:name).scoped_to(:election_id) }
    #   it { should validate_uniqueness_of(:slug).scoped_to(:election_id) }
    # end
  end

  context ".as_json" do
    let(:election) { create(:election) }
    let(:constituency) { create(:constituency, election: election, name: "Test North West", abbreviation: "Test N.W.") }
    let(:expected_json) do
      {
        name: "Test North West",
        abbreviation: "Test N.W.",
        slug: "test-north-west"
      }
    end

    subject { constituency.as_json }

    it "returns the expected json" do
      expect(subject). to eq expected_json
    end
  end
end
