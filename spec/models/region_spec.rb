require "spec_helper"
require "json"
require "csv"

RSpec.describe Region, type: :model do
  context "db fields" do
    it { should have_db_column(:id) }
    it { should have_db_column(:slug) }
    it { should have_db_column(:created_at) }
    it { should have_db_column(:updated_at) }
  end

  context "validation" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:slug) }

    context "uniqueness" do
      subject { create(:region) }
      it { should validate_uniqueness_of(:name) }
      it { should validate_uniqueness_of(:slug) }
    end
  end
end
