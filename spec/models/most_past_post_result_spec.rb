require "spec_helper"

RSpec.describe MostPastPostResult, type: :model do
  context "validation" do
    it { should validate_numericality_of(:threshold).only_integer.is_greater_than(0).allow_nil }
  end

  describe ".clean" do
    let(:result_calculations_spy) { instance_double("ActiveRecord::Associations::CollectionProxy", destroy_all: true) }
    let(:seats_spy) { instance_double("ActiveRecord::Associations::CollectionProxy", destroy_all: true) }
    let(:election) { create(:election, :with_constituencies, :with_results) }
    let(:mpp_result) { election.most_past_post_result }

    before do
      allow(mpp_result).to receive(:result_calculations).and_return(result_calculations_spy)
      allow(mpp_result).to receive(:seats).and_return(seats_spy)
      mpp_result.threshold = 1000
      mpp_result.clean
    end

    it "destroys all calculations" do
      expect(result_calculations_spy).to have_received(:destroy_all)
    end

    it "destroys all seats" do
      expect(seats_spy).to have_received(:destroy_all)
    end

    it "resets the threshold" do
      expect(mpp_result.threshold).to be_nil
    end
  end

  describe ".as_json" do
    let(:election) { create(:election, :with_constituencies, :with_results) }
    let(:mpp_result) { election.most_past_post_result }
    let(:expected_keys) { %i[id election_id created_at updated_at uri threshold party_proportions seats changed_seats] }

    before do
      allow(mpp_result).to receive(:party_proportions).and_return(details: "truncated")
    end

    subject { mpp_result.as_json }

    it "returns the expected hash" do
      expect(subject).to be_a Hash
      expect(subject.keys).to eq expected_keys
    end
  end

  describe ".changed_seats" do
    let(:election) { create(:election, :with_constituencies, :with_results) }
    let(:mpp_result) { election.most_past_post_result }

    subject { mpp_result.changed_seats }

    it "returns a MPTP::ChangedSeats instance" do
      expect(subject).to be_a MPTP::ChangedSeats
    end
  end

  describe ".mpp_constituencies" do
    before do
      load Rails.root.join("db/seeds/gb-general-election-2015-with-calculations.rb")
    end

    let(:election_slug) { "gb-general-election-2015" }
    let(:election) { Election.find_by!(slug: election_slug) }
    let(:mpp_result) { election.most_past_post_result }

    subject { mpp_result.mpp_constituencies }

    it "returns an array of constituencies" do
      expect(subject).to be_an Array
      expect(subject.size).to be_positive
      expect(subject).to all(be_a Constituency)
    end
  end

  describe ".fpp_seats" do
    before do
      load Rails.root.join("db/seeds/gb-general-election-2015-with-calculations.rb")
    end

    let(:election_slug) { "gb-general-election-2015" }
    let(:election) { Election.find_by!(slug: election_slug) }
    let(:mpp_result) { election.most_past_post_result }

    subject { mpp_result.fpp_seats }

    it "returns a collection of seats" do
      expect(subject).to be_a ActiveRecord::AssociationRelation
      expect(subject.size).to be_positive
      expect(subject).to all(be_a Seat)
    end
  end

  describe ".fpp_seats_by_party" do
    before do
      load Rails.root.join("db/seeds/gb-general-election-2015-with-calculations.rb")
    end

    let(:election_slug) { "gb-general-election-2015" }
    let(:election) { Election.find_by!(slug: election_slug) }
    let(:mpp_result) { election.most_past_post_result }
    let(:party) { Party.find_by!(slug: "snp") }

    subject { mpp_result.fpp_seats_by_party(party) }

    it "returns the expected fpp seats" do
      expect(subject).to be_a ActiveRecord::AssociationRelation
      expect(subject.size).to eq 56
      subject.each do |seat|
        expect(seat).to be_a Seat
        expect(seat.candidate.party).to eq party
      end
    end
  end

  describe ".mpp_parties" do
    before do
      load Rails.root.join("db/seeds/gb-general-election-2015-with-calculations.rb")
    end

    let(:election_slug) { "gb-general-election-2015" }
    let(:election) { Election.find_by!(slug: election_slug) }
    let(:mpp_result) { election.most_past_post_result }

    subject { mpp_result.mpp_parties }

    it "returns an array of parties" do
      expect(subject).to be_an Array
      expect(subject.size).to be_positive
      expect(subject).to all(be_a Party)
    end
  end

  describe ".mpp_party_constituencies" do
    before do
      load Rails.root.join("db/seeds/gb-general-election-2015-with-calculations.rb")
    end

    let(:election_slug) { "gb-general-election-2015" }
    let(:election) { Election.find_by!(slug: election_slug) }
    let(:mpp_result) { election.most_past_post_result }

    subject { mpp_result.mpp_party_constituencies }

    it "returns an array of party attributes with ranked candidates" do
      expect(subject).to be_an Array
      expect(subject.size).to eq 6
      expect(subject).to all(be_a Hash)
      expect(subject).to all(have_key(:ranked_candidates))
    end
  end

  describe ".party_rankings" do
    before do
      load Rails.root.join("db/seeds/gb-general-election-2015-with-calculations.rb")
    end

    let(:election_slug) { "gb-general-election-2015" }
    let(:election) { Election.find_by!(slug: election_slug) }
    let(:mpp_result) { election.most_past_post_result }

    subject { mpp_result.party_rankings }

    it "returns an array of party attributes with ranked candidates" do
      expect(subject).to be_an Array
      expect(subject.size).to eq 6
      expect(subject).to all(be_a Hash)
      expect(subject).to all(have_key(:ranked_candidates))
    end
  end

  describe ".party_proportions" do
    let(:election) { create(:election, :with_constituencies, :with_results) }
    let(:mpp_result) { election.most_past_post_result }
    let(:proportion_spy) { class_double(MPTP::PartyProportionCalculator).as_stubbed_const(new: true) }

    before do
      allow(MPTP::PartyProportionCalculator).to receive(:new).and_return(proportion_spy)
    end

    it "returns a MPTP::PartyProportionCalculator instance" do
      expect(proportion_spy).to receive(:new).with(election)
      mpp_result.party_proportions
    end
  end

  describe ".wasted_votes" do
    let(:election) { create(:election, :with_constituencies, :with_results) }
    let(:mpp_result) { election.most_past_post_result }
    let(:wasted_votes_spy) { instance_double(Stats::WastedMPPVotes, calculate: 1) }

    before do
      allow(Stats::WastedMPPVotes).to receive(:new).and_return(wasted_votes_spy)
    end

    it "returns a MPTP::PartyProportionCalculator instance" do
      expect(wasted_votes_spy).to receive(:calculate)
      mpp_result.wasted_votes
    end
  end
end
