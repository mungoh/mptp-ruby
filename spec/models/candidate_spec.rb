require "spec_helper"

RSpec.describe Candidate, type: :model do
  context "db fields" do
    it { should have_db_column(:id) }
    it { should have_db_column(:name) }
    it { should have_db_column(:slug) }
    it { should have_db_column(:votes) }
    it { should have_db_column(:created_at) }
    it { should have_db_column(:updated_at) }
    it { should belong_to(:party) }
    it { should belong_to(:constituency) }
  end

  context "validation" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:slug) }

    context "uniqueness" do
      let(:election) { create(:election) }
      let(:constituency) { create(:constituency, election: election) }
      let(:name) { "Very Unique" }

      subject { build(:candidate, constituency: constituency, name: name) }

      before do
        create(:candidate, constituency: constituency, name: name)
      end

      it { should validate_uniqueness_of(:name).scoped_to(:constituency_id) }
    end
  end

  context ".as_json" do
    let(:election) { create(:election) }
    let(:constituency) { create(:constituency, election: election) }
    let(:party) { create(:party) }
    let(:candidate) { create(:candidate, constituency: constituency, name: "Test McTestFace", slug: "test-mctest-face", votes: 1, party: party) }
    let(:expected_json) do
      {
        name: "Test McTestFace",
        slug: "test-mctestface",
        votes: 1
      }
    end

    subject { candidate.as_json }

    it "returns the expected json" do
      expect(subject). to eq expected_json
    end
  end
end
