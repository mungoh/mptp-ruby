require "spec_helper"

RSpec.describe Party, type: :model do
  context "db fields" do
    it { should have_db_column(:id) }
    it { should have_db_column(:name) }
    it { should have_db_column(:abbreviation) }

    it { should have_db_column(:created_at) }
    it { should have_db_column(:updated_at) }
    it { should have_many(:candidates) }
  end

  context "validation" do
    it { should validate_presence_of(:name) }

    context "uniqueness" do
      subject { create(:party) }
      it { should validate_uniqueness_of(:name) }
      it { should validate_uniqueness_of(:abbreviation).allow_nil }
    end
  end
end
