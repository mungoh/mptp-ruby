require "spec_helper"

RSpec.describe ResultCalculation, type: :model do
  let(:election_slug) { "gb-general-election-2015" }

  context "db fields" do
    it { should have_db_column(:id) }
    it { should have_db_column(:order_seq) }
    it { should have_db_column(:description) }
    it { should have_db_column(:calculation_type) }
    it { should have_db_column(:created_at) }
    it { should have_db_column(:updated_at) }
    it { should belong_to(:election_result) }
    it { should have_many(:seat_calculations) }
    it { should have_many(:seats) }
  end

  context ".as_json" do
    before do
      load Rails.root.join("db/seeds/gb-general-election-2015-with-calculations.rb")
    end

    subject do
      election = Election.find_by!(slug: election_slug)
      election.most_past_post_result.result_calculations.first
    end

    it "returns the expected hash" do
      expect(subject.as_json).to be_a Hash
      expect(subject.as_json.keys).to eq %i[order_seq calculation_type description seats]
    end
  end
end
