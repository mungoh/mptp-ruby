require "spec_helper"

RSpec.describe ElectionResult, type: :model do
  context "db fields" do
    it { should have_db_column(:id) }
    it { should have_db_column(:type) }
    it { should have_db_column(:threshold) }
    it { should have_db_column(:created_at) }
    it { should have_db_column(:updated_at) }
    it { should belong_to(:election) }
    it { should have_many(:seats) }
  end

  describe ".wasted_votes raises a NotImplementedError" do
    subject { ElectionResult.new.wasted_votes }

    it { expect { subject }.to raise_error NotImplementedError }
  end
end
