require "spec_helper"

RSpec.describe Seat, type: :model do
  context "db fields" do
    it { should have_db_column(:id) }
    it { should have_db_column(:metadata) }
    it { should have_db_column(:created_at) }
    it { should have_db_column(:updated_at) }
    it { should belong_to(:election_result) }
    it { should belong_to(:candidate) }
    it { should belong_to(:constituency) }
  end
end
