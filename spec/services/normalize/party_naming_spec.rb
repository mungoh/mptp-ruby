require "spec_helper"

describe Normalize::PartyNaming do
  describe ".normalize" do
    subject { described_class.new.normalize(party) }

    context "with unnormalized entry" do
      let(:party) { attributes_for(:party) }

      it "should return the expected attributes" do
        expect(subject).to eq party
      end
    end

    context "with matching name" do
      let(:party) { attributes_for(:party, name: "Labour") }
      let(:expected_party) { attributes_for(:party, name: "Labour", short_name: "Lab", abbreviation: "LAB", colour: "#ef1c27") }

      it "should return the expected attributes" do
        expect(subject).to eq expected_party
      end
    end

    context "with matching alias" do
      let(:party) { attributes_for(:party, name: "Labour and Co-operative") }
      let(:expected_party) { attributes_for(:party, name: "Labour", short_name: "Lab", abbreviation: "LAB", colour: "#ef1c27") }

      it "should return the expected attributes" do
        expect(subject).to eq expected_party
      end
    end
  end
end
