require "spec_helper"

describe Normalize::ConstituencyNaming do
  describe ".normalize" do
    subject { described_class.new.normalize(constituency_name) }

    context "with unnormalized entry" do
      let(:constituency_name) { "Nowhereshire North" }

      it "should return the expected attributes" do
        expect(subject).to eq constituency_name
      end
    end

    context "with matching name" do
      let(:constituency_name) { "North West Leicestershire" }

      it "should return the expected name" do
        expect(subject).to eq constituency_name
      end
    end

    context "with matching alias" do
      let(:constituency_name) { "Leicestershire North West" }
      let(:expected_name) { "North West Leicestershire" }

      it "should return the expected attributes" do
        expect(subject).to eq expected_name
      end
    end
  end

  describe ".abbreviate" do
    context "with unnormalized entry" do
      let(:constituency_name) { "Here and Nowhereshire North East" }

      subject { described_class.new.abbreviate(constituency_name) }

      it "should return the expected abbreviated name" do
        expect(subject).to eq "Here & Nowhereshire N.E."
      end
    end
  end
end
