require "spec_helper"

describe Export::ElectionsExporter do
  let(:export_target_path) { Rails.root.join("tmp/export") }
  let(:pending_year) { 3.months.from_now.year }

  before do
    load Rails.root.join("db/seeds/gb-general-election-2015-with-calculations.rb")
    Election.find_or_create_by(
      country: "GB",
      polling_date: 3.months.from_now,
      election_type: :general_election
    )
  end

  describe ".run" do
    let(:exporter) { described_class.new(dir: export_target_path) }
    let(:expected_files) do
      [
        "/elections.json",
        "/elections/gb-general-election-2015/results/fpp.json",
        "/elections/gb-general-election-2015/results/mpp.json",
        "/elections/gb-general-election-2015/most-past-post/party-rankings.json",
        "/elections/gb-general-election-2015/most-past-post/parties.json",
        "/elections/gb-general-election-2015/most-past-post/calculations.json",
        "/elections/gb-general-election-2015.json",
        "/elections/gb-general-election-#{pending_year}.json",
        "/geo/map-data-1992.json",
        "/geo/map-data-2005.json",
        "/geo/map-data-latest.json",
        "/geo/map-data-1997.json",
        "/geo/map-data-2001.json",
        "/metadata.json"
      ]
    end

    subject { exporter.run }

    it "generates the expected files" do
      subject
      filelist = Dir.glob("#{export_target_path}/**/*.json").collect { |f| f.gsub(export_target_path.to_s, "") }
      expect(filelist.sort).to eq expected_files.sort
    end
  end
end
