require "spec_helper"

describe Stats::WastedMPPVotes do
  let(:election_slug) { "gb-general-election-2015" }

  before(:all) do
    DatabaseCleaner.clean
    load Rails.root.join("db/seeds/gb-general-election-2015-no-calculations.rb")
  end

  after(:all) { DatabaseCleaner.clean }

  describe ".calculate" do
    let(:election) { Election.find_by!(slug: election_slug) }
    let(:mpp_result) { MostPastPostResult.find_or_create_by!(election: election) }

    subject do
      mpp_result.threshold = election.calculate_mpp_threshold
      mpp_result.save!

      described_class.new(election.id, mpp_result.mpp_parties.collect(&:id)).calculate
    end

    let(:expected_total) { 662110 }

    it "should return the correct value" do
      expect(subject).to eq expected_total
    end
  end
end
