require "spec_helper"

describe Stats::WastedFPPVotes do
  before do
    load Rails.root.join("db/seeds/gb-general-election-2015-no-calculations.rb")
  end

  describe ".calculate" do
    let(:election_slug) { "gb-general-election-2015" }
    let(:election) { Election.find_by!(slug: election_slug) }

    subject { described_class.new(election.id).calculate }

    let(:expected_total) { 14941159 }

    it "should return the correct value" do
      expect(subject).to eq expected_total
    end
  end
end
