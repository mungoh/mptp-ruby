require "spec_helper"

describe MPTP::ChangedSeats do
  before do
    load Rails.root.join("db/seeds/gb-general-election-2015-with-calculations.rb")
  end

  describe ".as_json" do
    let(:election_slug) { "gb-general-election-2015" }
    let(:election) { Election.find_by!(slug: election_slug) }

    subject { described_class.new(election.id).as_json }

    it "returns the expected array" do
      expect(subject).to be_an Array
      expect(subject.size).to eq 163
      subject.each do |entry|
        expect(entry.keys).to eq %i[constituency from_party to_party]
      end
    end
  end
end
