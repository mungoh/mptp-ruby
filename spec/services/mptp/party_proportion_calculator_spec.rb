require "spec_helper"

RSpec.describe MPTP::PartyProportionCalculator do
  before do
    load Rails.root.join("db/seeds/gb-general-election-2015-with-calculations.rb")
  end

  let(:election_slug) { "gb-general-election-2015" }
  let(:election) { Election.find_by!(slug: election_slug) }
  let(:expected_proportions) do
    {
      "conservative" => { deficit: 0, num_per_round: 10, num_seats: 240, percent: 38.23158198320709, surplus: 90 },
      "green" => { deficit: 24, num_per_round: 1, num_seats: 25, percent: 3.9167750186063985, surplus: 0 },
      "labour" => { deficit: 0, num_per_round: 8, num_seats: 199, percent: 31.625051051924242, surplus: 33 },
      "liberal-democrat" => { deficit: 43, num_per_round: 2, num_seats: 51, percent: 8.174113866996793, surplus: 0 },
      "snp" => { deficit: 0, num_per_round: 1, num_seats: 31, percent: 4.921001175644909, surplus: 25 },
      "ukip" => { deficit: 81, num_per_round: 3, num_seats: 82, percent: 13.131476903620564, surplus: 0 }
    }
  end

  subject { described_class.new(election) }

  describe ".to_h" do
    it "returns the expected hash" do
      expect(subject.to_h).to eq expected_proportions
    end
  end
end
