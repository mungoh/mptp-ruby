require "spec_helper"

RSpec.describe MPTP::ElectionLoader do
  describe ".new" do
    context "with invalid year" do
      it "should raise error" do
        expect { described_class.new(polling_date: "1899-12-31") }.to raise_error ArgumentError
      end
    end

    context "with valid data file" do
      let(:opts) do
        {
          country: "GB",
          polling_date: "2001-06-07",
          election_type: :general_election
        }
      end

      subject { described_class.new(opts) }

      it "should be ok" do
        expect(subject).to be_a_kind_of described_class
        expect(subject.election).to be_a_kind_of Election
        expect(subject.election.polling_date).to eq Date.parse("2001-06-07")
      end
    end
  end

  describe ".run" do
    let(:opts) do
      {
        country: "GB",
        polling_date: polling_date,
        election_type: :general_election
      }
    end

    context "with non-existent data" do
      let(:polling_date) { "2000-01-01" }

      subject { described_class.new(opts) }

      it "throws an ArgumentError" do
        expect { subject.run }.to raise_error ArgumentError
      end
    end

    context "with valid data" do
      let(:stubbed_stats) { { stats: "truncated" } }
      let(:election) { create(:election, opts) }

      before do
        allow(election).to receive(:generate_results).and_return(true)
        allow(election).to receive(:clean).and_return(true)
        allow(election).to receive(:stats).and_return(stubbed_stats)
        allow(Election).to receive(:find_or_initialize_by).and_return(election)
      end

      subject { described_class.new(opts).run }

      context "with 2001 data structure" do
        let(:polling_date) { "2001-06-07" }

        it "loads the expected data" do
          expect(subject).to eq stubbed_stats
          expect(election.constituencies.size).to eq 659
          expect(election.candidates.size).to eq 3318
          expect(election).to have_received(:clean)
          expect(election).to have_received(:generate_results)
        end
      end

      context "with 2005 data structure" do
        let(:polling_date) { "2005-05-05" }

        it "loads the expected data" do
          expect(subject).to eq stubbed_stats
          expect(election.constituencies.size).to eq 646
          expect(election.candidates.size).to eq 3554
          expect(election).to have_received(:clean)
          expect(election).to have_received(:generate_results)
        end
      end

      context "with 2010 data structure" do
        let(:polling_date) { "2010-05-06" }

        it "loads the expected data" do
          expect(subject).to eq stubbed_stats
          expect(election.constituencies.size).to eq 650
          expect(election.candidates.size).to eq 4150
          expect(election).to have_received(:clean)
          expect(election).to have_received(:generate_results)
        end
      end

      context "with 2015 data structure" do
        let(:polling_date) { "2015-05-07" }

        it "loads the expected data" do
          expect(subject).to eq stubbed_stats
          expect(election.constituencies.size).to eq 650
          expect(election.candidates.size).to eq 3971
          expect(election).to have_received(:clean)
          expect(election).to have_received(:generate_results)
        end
      end
    end
  end
end
