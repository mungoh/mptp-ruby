require "spec_helper"

RSpec.describe MPTP::ResultCalculator do
  describe ".mpp_result" do
    let(:election) { create(:election, :with_constituencies, :with_results) }

    subject { described_class.new(election).mpp_result }

    it "returns a MostPastPostResult" do
      expect(subject).to be_a MostPastPostResult
    end
  end

  describe ".party_proportions" do
    let(:election) { create(:election, :with_constituencies, :with_results) }
    let(:proportion_spy) { class_double(MPTP::PartyProportionCalculator).as_stubbed_const(new: true) }

    before do
      allow(MPTP::PartyProportionCalculator).to receive(:new).and_return(proportion_spy)
    end

    subject { described_class.new(election) }

    it "returns a PartyProportionCalculator" do
      expect(proportion_spy).to receive(:new).with(election)
      subject.party_proportions
    end
  end

  describe ".run" do
    before do
      load Rails.root.join("db/seeds/gb-general-election-2015-with-calculations.rb")
    end

    context "with gb-general-election-2015 results loaded" do
      let(:election_slug) { "gb-general-election-2015" }

      it "re-generates the expected results" do
        election = Election.find_by!(slug: election_slug)
        calculator = described_class.new(election)
        expect(calculator.run).to eq true
        expect(election.most_past_post_result.seats.count).to eq 650
      end
    end
  end
end
