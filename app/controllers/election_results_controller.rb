class ElectionResultsController < ApplicationController
  def show
    election = Election.find(show_params[:election_id])
    fresh_when(etag: election, last_modified: election.updated_at, public: true)

    results =
      if show_params[:result_type] == "fpp"
        election.first_past_post_result
      elsif show_params[:result_type] == "mpp"
        election.most_past_post_result
      end

    render json: results.to_json
  end

  private

  def show_params
    params.require(:election_id)
    params.require(:result_type)
    params
  end
end
