module MostPastPost
  class CalculationsController < ApplicationController
    def index
      election = Election.find(show_params[:election_id])
      fresh_when(etag: election, last_modified: election.updated_at, public: true)

      calculations = ResultCalculation.where(election_result_id: election.most_past_post_result.id)
                                      .order(:order_seq)

      render json: calculations.collect(&:as_json).to_json
    end

    def party_rankings
      election = Election.find(show_params[:election_id])
      fresh_when(etag: election, last_modified: election.updated_at, public: true)

      render json: election.most_past_post_result.party_rankings.to_json
    end

    private

    def show_params
      params.require(:election_id)
      params
    end
  end
end
