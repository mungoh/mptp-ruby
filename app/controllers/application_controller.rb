class ApplicationController < ActionController::API
  before_action :set_headers

  def set_headers
    response.headers["Content-Type"] = "application/json"
  end

  rescue_from(Exception) do |e|
    case e
    when ActionController::ParameterMissing
      error = { missing_parameter: e.message }
      status = :bad_request

    when ActiveRecord::RecordNotFound
      error = { record_not_found: e.message }
      status = :not_found

    else
      error = { exception: e.message }
      status = :bad_request
    end

    render json: { error: error }, status: status
  end
end
