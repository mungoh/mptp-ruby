class IndexController < ApplicationController
  def index
    index = {
      object: "elections",
      path: "/elections"
    }

    render json: index
  end
end
