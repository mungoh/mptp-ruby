class ElectionsController < ApplicationController
  def index
    fresh_when(etag: Election.last, last_modified: Election.last&.updated_at, public: true)

    render json: Election.all.collect(&:to_json)
  end

  def show
    election = Election.friendly.find(elections_params[:id])
    fresh_when(etag: election, last_modified: election.updated_at, public: true)

    render json: election.to_json
  end

  private

  def elections_params
    params.require(:id)
    params
  end
end
