module Export
  class ElectionsExporter
    def initialize(options = {})
      @target_dir = options[:dir]
      if File.directory?(@target_dir)
        FileUtils.rm_r Dir.glob("#{@target_dir}/*")
      else
        FileUtils.mkdir_p(@target_dir)
      end
    end

    def run
      election_path = @target_dir.join("elections")
      FileUtils.mkdir_p(election_path)
      elections.each { |election| export_election(election, election_path) }
      generate_index
      export_geo_data
      export_metadata
      puts "#{elections.count} elections generated"
    end

    private

    def export_election(election, election_path)
      export_election_index(election, election_path)
      return if election.pending?

      export_election_results(election, election_path)
      export_election_calculations(election, election_path)
      export_election_parties(election, election_path)
      export_election_party_rankings(election, election_path)
    end

    def export_election_index(election, election_path)
      FileUtils.mkdir_p(election_path)
      dest = election_path.join("#{election.slug}.json")
      File.open(dest, "w") { |file| file.puts election.to_json }
    end

    def export_election_results(election, election_path)
      results_dir = election_path.join(election.slug.to_s, "results")
      FileUtils.mkdir_p(results_dir)
      File.open(results_dir.join("fpp.json"), "w") { |file| file.puts election.first_past_post_result.to_json }
      File.open(results_dir.join("mpp.json"), "w") { |file| file.puts election.most_past_post_result.to_json }
    end

    def export_election_calculations(election, election_path)
      calculation_dir = election_path.join(election.slug.to_s, "most-past-post")
      FileUtils.mkdir_p(calculation_dir)

      calculations = ResultCalculation.where(election_result_id: election.most_past_post_result.id)
                                      .order(:order_seq)
      File.open(calculation_dir.join("calculations.json"), "w") do |file|
        file.puts calculations.collect(&:as_json).to_json
      end
    end

    def export_election_parties(election, election_path)
      calculation_dir = election_path.join(election.slug.to_s, "most-past-post")
      party_seat_totals = election.most_past_post_result.seat_totals

      election_party_stats = party_seat_totals.collect do |entry|
        Party
          .find_by!(slug: entry[0])
          .as_json
          .merge(num_seats: entry[1])
      end
      File.open(calculation_dir.join("parties.json"), "w") do |file|
        file.puts election_party_stats.to_json
      end
    end

    def export_election_party_rankings(election, election_path)
      calculation_dir = election_path.join(election.slug.to_s, "most-past-post")
      FileUtils.mkdir_p(calculation_dir)

      File.open(calculation_dir.join("party-rankings.json"), "w") do |file|
        file.puts election.most_past_post_result.party_rankings.to_json
      end
    end

    def generate_index
      elections_index = { elections: elections.collect(&:as_json) }
      File.open(@target_dir.join("elections.json"), "w") do |file|
        file.puts elections_index.to_json
      end
    end

    def export_geo_data
      geo_dir = @target_dir.join("geo")
      FileUtils.mkdir_p(geo_dir)
      src_dir = Rails.root.join("config/geo/gb")
      FileUtils.cp(src_dir.glob("*.json"), geo_dir)
    end

    def export_metadata
      File.open(@target_dir.join("metadata.json"), "w") do |file|
        file.puts({ data_generated: Time.current }.to_json)
      end
    end

    def elections
      @elections ||= Election.all.order(polling_date: :desc)
    end
  end
end
