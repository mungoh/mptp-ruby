require "csv"

module MPTP
  class ElectionLoader
    attr_reader :election

    def initialize(options = {})
      polling_date = Date.parse(options[:polling_date])
      raise ArgumentError, "year invalid #{polling_date}" unless polling_date&.year.to_i > 1900

      @election = Election.find_or_initialize_by(
        polling_date: polling_date,
        election_type: options[:election_type],
        country: options[:country].to_s.upcase
      )
    end

    def run
      raise ArgumentError, "data not available for year #{election.polling_date.year}" unless File.exist?(seed_path("#{year}.csv"))

      election.clean
      create_constituencies
      create_parties
      create_candidates

      election.generate_results
      election.stats
    end

    private

    def year
      @year ||= @election.year
    end

    def seed_data
      load_csv(seed_path("#{year}.csv"))
    end

    def load_csv(filepath)
      csv = CSV.new(File.open(filepath), headers: true, header_converters: :symbol, converters: :all)
      csv.to_a.map(&:to_hash)
    end

    def seed_path(filename)
      Rails.root.join("db", "seeds", "elections", @election.country.downcase, @election.election_type.to_s.pluralize, filename)
    end

    def create_constituencies
      @election.constituencies = constituency_seeds.collect { |row| create_constituency(row) }
      @election.save!
      Rails.logger.info "created #{@election.constituencies.size} constituencies"
      @election.constituencies
    end

    def constituency_seeds
      seeds =
        if File.exist?(seed_path("#{year}_constituencies.csv"))
          load_csv(seed_path("#{year}_constituencies.csv"))
        else
          seed_data
        end
      seeds.delete_if { |row| row[:constituency].to_s.strip.blank? }
    end

    def create_constituency(attributes)
      constituency_name = constituency_naming.normalize(attributes[:constituency].strip)
      constituency = Constituency.find_or_initialize_by(name: constituency_name, election_id: @election.id)
      constituency.abbreviation = constituency_naming.abbreviate(constituency_name)
      constituency.size = attributes[:electorate]
      constituency.ons_id = attributes[:ons_id] if attributes.key?(:ons_id)
      constituency.region = create_region(attributes[:region]) if attributes[:region]

      constituency
    end

    def create_parties
      party_seeds
        .compact
        .uniq
        .collect { |row| find_or_create_party(row) }
    end

    def party_seeds
      return party_seeds_csv if File.exist?(seed_path("#{year}_parties.csv"))

      seed_data
        .delete_if { |row| row[:party].nil? || row[:party].strip.blank? }
        .collect { |row| { name: row[:party].strip } }
    end

    def party_seeds_csv
      load_csv(seed_path("#{year}_parties.csv"))
        .collect { |row| { name: row[:party], short_name: row[:short_name], abbreviation: row[:abbreviation], colour: row[:colour] } }
    end

    def find_or_create_party(attributes)
      Party.find_or_create_by(
        party_naming.normalize(attributes)
      )
    end

    def party_naming
      @party_naming ||= Normalize::PartyNaming.new
    end

    def constituency_naming
      @constituency_naming ||= Normalize::ConstituencyNaming.new
    end

    def create_candidates
      constituency_name = nil

      seed_data.collect do |row|
        # constituency_name can be blank/nil depending on how the data is structured
        # re-used on subsequent rows until it changes
        constituency_name = constituency_naming.normalize(row[:constituency].strip) unless row[:constituency].to_s.strip.blank?
        next if row[:candidate].blank?

        create_constituency_candidate(constituency_name, row)
      end
    end

    def create_constituency_candidate(constituency_name, row)
      raise "no constituency #{row}" if constituency_name.blank?

      constituency = @election.constituencies.find_by!(name: constituency_name)
      party = find_or_create_party(name: row[:party])

      candidate = Candidate.new(name: row[:candidate], constituency: constituency, party: party, votes: row[:votes])

      candidate.save!
      candidate
    end

    def create_region(region_name)
      Region.find_or_create_by(name: region_name)
    end
  end
end
