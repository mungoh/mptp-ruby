module MPTP
  class ChangedSeats
    attr_reader :election_id

    def initialize(election_id)
      @election_id = election_id
    end

    def calculate
      Seat.select("*")
          .from(Arel.sql("(#{query}) as seats"))
    end

    def as_json(*)
      calculate
        .collect do |seat|
          fpp_candidate = Seat.find_by(election_result: fpp_result, constituency: seat.constituency).candidate
          {
            constituency: seat.constituency.slug,
            from_party: fpp_candidate&.party&.slug || "none",
            to_party: seat.candidate&.party&.slug || "none"
          }
        end
    end

    private

    def fpp_result
      @fpp_result ||= FirstPastPostResult.find_by(election_id: election_id)
    end

    def query
      <<-SQL
        WITH election AS (
            SELECT * FROM elections WHERE id = #{election_id}
        ),
        fpp_result AS (
            SELECT election_results.id
            FROM election_results, election
            WHERE election_results.election_id = election.id
            AND election_results.type = 'FirstPastPostResult'
        ),
        mpp_result AS (
            SELECT election_results.id
            FROM election_results, election
            WHERE election_results.election_id = election.id
            AND election_results.type = 'MostPastPostResult'
        ),
        fpp_seats AS (
            SELECT seats.*
            FROM fpp_result, seats
            WHERE seats.election_result_id = fpp_result.id
        )
        SELECT seats.*
        FROM  mpp_result, seats, fpp_seats
        WHERE seats.election_result_id = mpp_result.id
        AND seats.constituency_id = fpp_seats.constituency_id
        AND seats.candidate_id != fpp_seats.candidate_id
      SQL
    end
  end
end
