module MPTP
  class ReassignChangedSeats
    attr_reader :election
    attr_accessor :available_constituency_ids, :reassignments

    delegate :constituencies, to: :election
    delegate :most_past_post_result, to: :election
    delegate :mpp_party_ids, to: :most_past_post_result

    def initialize(election)
      @election = election
    end

    def calculate
      @available_constituency_ids = changed_constituency_ids.dup
      @reassignments = []
      changed_constituencies.each { |constituency| calculate_constituency_reassignment(constituency) }

      @reassignments
    end

    private

    def calculate_constituency_reassignment(constituency)
      seat = most_past_post_result.seats.find_by(constituency: constituency)
      candidates = constituency_candidates(constituency)
      candidates.each do |candidate|
        next unless @available_constituency_ids.include? constituency.id

        target_pair = find_pair(constituency, seat.candidate.party, candidate.party)
        next unless target_pair

        @reassignments << target_pair
        break
      end
    end

    def find_pair(constituency, current_mpp_party, target_party)
      current_party_candidates = better_placed_candidates.where(party: current_mpp_party).where("constituency_id != ?", constituency.id)
      target_seats = find_target_seats(target_party)
      return nil if target_seats.blank?

      target_candidate = current_party_candidates.where("constituency_id IN (?)", target_seats.collect(&:constituency_id)).first
      return nil unless target_candidate

      @available_constituency_ids.delete(constituency.id)
      @available_constituency_ids.delete(target_candidate.constituency_id)
      [
        { constituency: constituency, from_party: current_mpp_party, to_party: target_party },
        { constituency: target_candidate.constituency, from_party: target_party, to_party: current_mpp_party }
      ]
    end

    def find_target_seats(target_party)
      most_past_post_result
        .seats
        .joins(:candidate)
        .where("candidates.constituency_id IN (?)", @available_constituency_ids)
        .where("candidates.party_id = ?", target_party.id)
    end

    def constituency_candidates(constituency)
      better_placed_candidates.select("candidates.*")
                              .joins(:constituency)
                              .where(constituency: constituency)
                              .order(Arel.sql("votes / (constituencies.size * 1.0) DESC"))
    end

    def changed_constituency_ids
      @changed_constituency_ids ||= changed_constituencies.collect(&:id)
    end

    def changed_constituencies
      @changed_constituencies ||=
        most_past_post_result
        .changed_seats
        .calculate
        .collect(&:constituency)
    end

    def better_placed_candidates
      @better_placed_candidates ||= MPTP::BetterPlacedCandidates.new(election.id).calculate
    end
  end
end
