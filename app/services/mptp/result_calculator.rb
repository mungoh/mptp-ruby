module MPTP
  class ResultCalculator
    attr_accessor :election, :assigned_seat_count

    def initialize(election)
      @election = election
      @assigned_seat_count = 0
    end

    def mpp_result
      election.most_past_post_result
    end
    delegate :mpp_parties, :mpp_party_constituencies, :mpp_constituencies, to: :mpp_result

    def party_proportions
      @party_proportions ||= PartyProportionCalculator.new(election)
    end
    delegate :party_seat_proportions, to: :party_proportions

    def run
      clean
      prepare

      assign_fpp_seats
      mpp_result.save

      generate_calculations
      mpp_result.save
      election.save

      swap_reciprocal_seats

      mpp_result.save!
    end

    private

    def generate_calculations
      order_seq = 0
      generate_calculation(mpp_result.fpp_seats, order_seq, :below_threshold, "assign outright first past post seats")

      @ranked_parties = mpp_party_constituencies.clone

      while remaining_constituencies_count.positive?
        order_seq += 1
        generate_calculation(
          assign_mpp_seat_batch,
          order_seq,
          :above_threshold,
          "most past post seat allocation: #{order_seq}"
        )
      end
    end

    def swap_reciprocal_seats
      reassignments = MPTP::ReassignChangedSeats.new(election).calculate.flatten

      until reassignments.blank?
        generate_swap_calculation(reassignments)
        election.reload
        reassignments = MPTP::ReassignChangedSeats.new(election).calculate.flatten
      end
    end

    def generate_swap_calculation(reassignments)
      order_seq = mpp_result.result_calculations.count
      result_calculation = ResultCalculation.find_or_initialize_by(
        election_result_id: mpp_result.id,
        order_seq: order_seq,
        calculation_type: :seat_reassignment,
        description: "most past post reciprocal re-allocations: #{order_seq}"
      )
      result_calculation.seats = reassignments.collect { |entry| reassign_swappable_seat(entry) }
      mpp_result.result_calculations << result_calculation
    end

    def reassign_swappable_seat(entry)
      constituency = entry[:constituency]
      candidate = constituency.candidates.find_by!(party: entry[:to_party])
      metadata = {
        reassignment: {
          from: {
            party: entry[:from_party]&.slug
          }
        }
      }
      mpp_result.assign_seat(constituency, candidate, metadata)
    end

    def assign_fpp_seats
      mpp_result.fpp_seats.each do |seat|
        Rails.logger.info "assign fpp seat: #{seat.constituency.name} >> #{seat.candidate.party.name}"
        mpp_result.assign_seat(seat.constituency, seat.candidate)
      end
    end

    def assign_mpp_seat_batch
      seat_batch = []
      (1..max_per_round).each do |round_idx|
        party_seat_proportions.each do |entry|
          next if round_idx > entry[:num_per_round]

          party = entry[:party]
          num_assigned = mpp_result.seats.joins(:candidate).where("candidates.party_id = ?", party.id).count

          seat_batch << assign_mpp_seat_to_party(party) if num_assigned < entry[:num_seats]
        end
      end

      seat_batch.compact
    end

    def assign_mpp_seat_to_party(party)
      candidate = next_party_candidate(party)
      raise "no candidate" unless candidate

      remove_other_constituency_candidates(candidate.constituency_id)
      mpp_result.assign_seat(candidate.constituency, candidate)
    end

    def next_party_candidate(party)
      idx = mpp_party_constituencies.find_index { |entry| entry[:id] == party.id }
      mpp_party_constituencies[idx][:ranked_candidates].shift
    end

    def remove_other_constituency_candidates(constituency_id)
      mpp_party_constituencies.each do |entry|
        entry[:ranked_candidates].delete_if { |candidate| candidate.constituency_id == constituency_id }
      end
    end

    def remaining_constituencies_count
      mpp_result.election.constituencies.count - assigned_seat_count
    end

    def max_per_round
      @max_per_round ||= party_seat_proportions.max_by { |p| p[:num_per_round] }[:num_per_round]
    end

    def generate_calculation(seats, order_seq, calculation_type, description)
      result_calculation = ResultCalculation.find_or_initialize_by(
        election_result_id: mpp_result.id,
        order_seq: order_seq
      )
      result_calculation.description = description
      result_calculation.calculation_type = calculation_type
      result_calculation.seats = seats
      @assigned_seat_count += seats.count
      mpp_result.result_calculations << result_calculation
    end

    def clean
      mpp_result.clean
    end

    def prepare
      raise "first past post result not available" unless election.first_past_post_result.present?
      raise "first past post results not generated" if election.first_past_post_result.seats.blank?

      mpp_result.threshold = election.calculate_mpp_threshold
      mpp_result.save!
    end
  end
end
