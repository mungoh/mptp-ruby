module MPTP
  class BetterPlacedCandidates
    attr_reader :election_id

    def initialize(election_id)
      @election_id = election_id
    end

    def calculate
      Candidate.select("*")
               .from(Arel.sql("(#{query}) as candidates"))
    end

    private

    def query
      <<-SQL
        WITH election AS (
        	SELECT * FROM elections WHERE id = #{election_id}
        ),
        fpp_result AS (
        	SELECT election_results.id
        	FROM election_results, election
        	WHERE election_results.election_id = election.id
        	AND election_results.type = 'FirstPastPostResult'
        ),
        mpp_result AS (
        	SELECT election_results.id
        	FROM election_results, election
        	WHERE election_results.election_id = election.id
        	AND election_results.type = 'MostPastPostResult'
        ),
        fpp_seats AS (
        	SELECT seats.constituency_id, constituencies.slug AS constituency_slug, seats.candidate_id
        	FROM fpp_result, seats, constituencies
        	WHERE seats.election_result_id = fpp_result.id
        	AND seats.constituency_id = constituencies.id
        ),
        mpp_changed_seats AS (
        	SELECT constituencies.id AS constituency_id, constituencies.slug AS constituency_slug, votes
        	FROM mpp_result, seats, constituencies, fpp_seats, candidates
        	WHERE seats.election_result_id = mpp_result.id
        	AND seats.constituency_id = constituencies.id
        	AND seats.constituency_id = fpp_seats.constituency_id
        	AND seats.candidate_id != fpp_seats.candidate_id
        	AND seats.candidate_id = candidates.id
        )
        SELECT candidates.*
        FROM mpp_changed_seats, candidates
        WHERE mpp_changed_seats.constituency_id = candidates.constituency_id
        AND candidates.votes > mpp_changed_seats.votes
      SQL
    end
  end
end
