module MPTP
  class PartyProportionCalculator
    attr_reader :party_seat_proportions, :election

    def initialize(election)
      @election = election
      calculate_seat_proportions
    end

    def to_h
      proportions = {}
      party_seat_proportions.each do |entry|
        proportions[entry[:party].slug] = entry.reject { |k, _v| k == :party }
      end
      proportions
    end

    private

    def mpp_result
      election.most_past_post_result
    end
    delegate :mpp_parties, :mpp_party_constituencies, :mpp_constituencies, to: :mpp_result

    def calculate_seat_proportions
      seat_proportions = mpp_parties.collect { |party| calculate_party_proportions(party) }
                                    .sort_by { |party| party[:num_seats] }

      remainder = mpp_constituencies.count - seat_proportions.inject(0) { |sum, p| sum + p[:num_seats] }
      calculate_num_per_round(
        apply_allocation_rounding(seat_proportions, remainder)
      )
      @party_seat_proportions = seat_proportions
    end

    def calculate_party_proportions(party)
      percent = party_percent(party)
      num_seats = ((mpp_constituencies.count / 100.0) * percent).round

      num_fpp_seats = election.first_past_post_result.party_seats(party).size

      {
        party: party,
        percent: percent,
        num_seats: num_seats,
        deficit: [num_seats - num_fpp_seats, 0].max,
        surplus: [num_fpp_seats - num_seats, 0].max
      }
    end

    def party_percent(party)
      (party.total / mpp_total_votes.to_f) * 100
    end

    def calculate_num_per_round(seat_proportions)
      min_seats = seat_proportions.min_by { |p| p[:num_seats] }[:num_seats].to_f
      seat_proportions.each { |p| p[:num_per_round] = (p[:num_seats] / min_seats).round }
      seat_proportions
    end

    def apply_allocation_rounding(seat_proportions, remainder)
      while remainder.positive?
        # assign rounding discrepancy to largest parties
        seat_proportions.each do |entry|
          break if remainder.zero?

          entry[:num_seats] += 1
          entry[:surplus] -= 1 if entry[:surplus].positive?
          remainder -= 1
        end
      end
      seat_proportions
    end

    def mpp_total_votes
      @mpp_total_votes ||= mpp_parties.inject(0) { |sum, p| sum + p.total }
    end
  end
end
