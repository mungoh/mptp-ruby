module Stats
  class WastedFPPVotes
    attr_reader :election_id

    def initialize(election_id)
      @election_id = election_id
    end

    def calculate
      Election.connection
              .select_one(Arel.sql(query))["total_wasted_votes"]
              .to_i
    end

    private

    def query
      <<-SQL
        WITH election_constituencies AS (
          SELECT constituencies.*
          FROM constituencies, elections
          WHERE constituencies.election_id = elections.id
          AND elections.id = #{election_id}
        ),
        ranked_candidates AS (
          SELECT candidates.id, candidates.constituency_id, candidates.slug, candidates.votes, rank() over (
            PARTITION BY constituency_id
            ORDER BY votes DESC
          ) AS rank
          FROM candidates, election_constituencies
          WHERE candidates.constituency_id = election_constituencies.id
        ),
        winners AS (
          SELECT ranked_candidates.*
          FROM ranked_candidates
          WHERE rank = 1
        ),
        runners_up AS (
          SELECT ranked_candidates.*
          FROM ranked_candidates
          WHERE rank = 2
        ),
        other_losers AS (
          SELECT ranked_candidates.*
          FROM ranked_candidates
          WHERE rank > 2
        ),
        constituency_wasted_votes AS (
          SELECT election_constituencies.id, election_constituencies.slug,
          winners.votes - runners_up.votes - 1 + SUM(other_losers.votes) AS wasted_votes
          FROM election_constituencies, winners, runners_up, other_losers
          WHERE election_constituencies.id = winners.constituency_id
          AND election_constituencies.id = runners_up.constituency_id
          AND election_constituencies.id = other_losers.constituency_id
          GROUP BY election_constituencies.id, election_constituencies.slug, winners.votes, runners_up.votes
          ORDER BY election_constituencies.slug
        )
        SELECT SUM(wasted_votes) AS total_wasted_votes
        FROM constituency_wasted_votes
      SQL
    end
  end
end
