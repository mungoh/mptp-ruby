module Stats
  class WastedMPPVotes
    attr_reader :election_id, :mpp_party_ids

    def initialize(election_id, mpp_party_ids)
      @election_id = election_id
      @mpp_party_ids = mpp_party_ids
    end

    def calculate
      Election.connection
              .select_one(Arel.sql(query))["total_wasted_votes"]
              .to_i
    end

    private

    def query
      <<-SQL
        WITH election_constituencies AS (
          SELECT constituencies.*
          FROM constituencies, elections
          WHERE constituencies.election_id = elections.id
          AND elections.id = #{election_id}
        ),
        ranked_candidates AS (
          SELECT candidates.id, candidates.party_id, candidates.constituency_id, candidates.slug, candidates.votes, rank() over (
            PARTITION BY constituency_id
            ORDER BY votes DESC
          ) AS rank
          FROM candidates, election_constituencies
          WHERE candidates.constituency_id = election_constituencies.id
        ),
        fpp_winners AS (
          SELECT ranked_candidates.*
          FROM ranked_candidates
          WHERE rank = 1
          AND ranked_candidates.party_id NOT IN (#{mpp_party_ids.join(',')})
        ),
        runners_up AS (
          SELECT ranked_candidates.*
          FROM ranked_candidates
          WHERE rank = 2
        ),
        other_losers AS (
          SELECT SUM(votes) AS other_total
          FROM ranked_candidates
          WHERE rank > 2
          AND ranked_candidates.party_id NOT IN (#{mpp_party_ids.join(',')})
        ),
        constituency_surplus_votes AS (
          SELECT election_constituencies.id, election_constituencies.slug,
          fpp_winners.votes - runners_up.votes - 1 AS winner_surplus
          FROM election_constituencies, fpp_winners, runners_up
          WHERE election_constituencies.id = fpp_winners.constituency_id
          AND election_constituencies.id = runners_up.constituency_id
          GROUP BY election_constituencies.id, election_constituencies.slug, fpp_winners.votes, runners_up.votes
          ORDER BY election_constituencies.slug
        )
        SELECT SUM(winner_surplus) + other_total AS total_wasted_votes
        FROM constituency_surplus_votes, other_losers
        GROUP BY other_losers.other_total
      SQL
    end
  end
end
