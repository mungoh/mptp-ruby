module Normalize
  class PartyNaming
    attr_accessor :normalized_data

    def initialize
      @normalized_data =
        YAML.load_file(Rails.root.join("config", "normalize", "gb", "normalized-parties.yml"))
            .deep_symbolize_keys[:parties]
    end

    def normalize(party_attributes)
      party_attributes.merge(
        normalized_attributes(party_attributes[:name])
      )
    end

    private

    def normalized_attributes(name)
      replacements = replacement_attributes(name)
      return {} if replacements.blank?

      {
        name: replacements[:name],
        short_name: replacements[:short_name],
        abbreviation: replacements[:abbreviation],
        colour: replacements[:colour]
      }.compact
    end

    def replacement_attributes(name)
      normalized_data.find do |entry|
        entry[:name] == name ||
          entry[:aliases].include?(name)
      end
    end
  end
end
