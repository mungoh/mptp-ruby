module Normalize
  class ConstituencyNaming
    attr_accessor :normalized_data

    def initialize
      @normalized_data =
        YAML.load_file(Rails.root.join("config", "normalize", "gb", "normalized-constituencies.yml"))
            .deep_symbolize_keys[:constituencies]
    end

    def normalize(name)
      name = name.to_s.gsub(/&/, "and")
      normalized_name(name)&.dig(:name) || name
    end

    def abbreviate(name)
      name.to_s
          .gsub(/ and( |$)/, " &\\1")
          .gsub(/(^| )North((,)? |$)/, "\\1N.\\2")
          .gsub(/(^| )South((,)? |$)/, "\\1S.\\2")
          .gsub(/(^| )East((,)? |$)/, "\\1E.\\2")
          .gsub(/(^| )West((,)? |$)/, "\\1W.\\2")
          .gsub(/(^| )Central((,)? |$)/, "\\1Cen.\\2")
          .gsub(/([NSEW]\.) ([NSEW]\.)/, "\\1\\2")
    end

    private

    def normalized_name(name)
      normalized_data.find { |entry| [entry[:name], entry[:alias].to_s].include? name }
    end
  end
end
