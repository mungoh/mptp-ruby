class ResultCalculation < ApplicationRecord
  belongs_to :election_result
  has_many :seat_calculations, dependent: :destroy
  has_many :seats, through: :seat_calculations

  enum calculation_type: %i[below_threshold above_threshold seat_reassignment]

  def as_json(*)
    {
      "order_seq": order_seq,
      "calculation_type": calculation_type.to_s,
      "description": description,
      "seats": seat_calculations.collect(&:seat).collect(&:as_json)
    }
  end
end
