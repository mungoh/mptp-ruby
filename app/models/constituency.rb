class Constituency < ApplicationRecord
  extend FriendlyId

  belongs_to :election
  belongs_to :region
  has_many :candidates, dependent: :destroy

  friendly_id :name, use: :scoped, scope: :election_id

  validates :name, uniqueness: { scope: :election_id }, presence: true
  validates :slug, uniqueness: { scope: :election_id }, presence: true

  def as_json(*)
    {
      "slug": slug,
      "name": name,
      "abbreviation": abbreviation
    }
  end
end
