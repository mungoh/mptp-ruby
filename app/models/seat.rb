class Seat < ApplicationRecord
  belongs_to :constituency
  belongs_to :candidate
  belongs_to :election_result
  has_one :seat_calculation, dependent: :destroy
  has_one :result_calculation, through: :seat_calculation

  scope :by_party, ->(party) { joins(:candidate).where("candidates.party_id = ?", party.id) }

  def as_json(*)
    {
      "constituency": {
        "slug": constituency.slug,
        "name": constituency.name,
        "abbreviation": constituency.abbreviation,
        "ons_id": constituency.ons_id
      },
      "party": {
        "abbreviation": candidate_party.abbreviation,
        "short_name": candidate_party.short_name,
        "slug": candidate_party.slug
      },
      "candidate": {
        "name": candidate.name
      },
      "metadata": metadata
    }.compact
  end

  private

  def candidate_party
    @candidate_party ||= candidate.party || Party::NONE
  end
end
