class ElectionResult < ApplicationRecord
  belongs_to :election
  validates :type, presence: true, inclusion: { in: %w[FirstPastPostResult MostPastPostResult] }

  has_many :seats, dependent: :destroy
  has_many :result_calculations, dependent: :destroy # only used for mpp results

  def party_groupings
    seats.group_by { |seat| seat.candidate.party || Party::NONE }
  end

  def party_seats(party)
    seats.by_party(party)
  end

  def stats
    {
      seat_totals: seat_totals,
      wasted_votes: wasted_votes
    }
  end

  def seat_totals
    Hash[
      party_groupings.collect do |party_result|
        [party_result[0].slug, party_result[1].size]
      end
    ].sort_by { |slug, num_seats| [-num_seats, slug] }
  end

  def wasted_votes
    raise NotImplementedError, "not implemented: wasted_votes"
  end

  def assign_seat(constituency, candidate, metadata = nil)
    seat = seats.find_or_initialize_by(constituency: constituency)
    seat.candidate = candidate
    seat.metadata = metadata if metadata
    seat.save!
    seat
  end

  def clean
    result_calculations.destroy_all
    seats.destroy_all
  end

  def uri
    result_type = type == "FirstPastPostResult" ? "fpp" : "mpp"
    "#{election.uri}/results/#{result_type}.json"
  end

  def as_json(*)
    as_summary_json
      .merge("seats": seats.collect(&:as_json))
  end

  def as_summary_json
    summary = {
      id: id,
      election_id: election_id,
      created_at: created_at,
      updated_at: updated_at,
      uri: uri
    }
    if type == "MostPastPostResult"
      summary[:threshold] = threshold
      summary[:party_proportions] = party_proportions&.to_h
    end

    summary
  end
end
