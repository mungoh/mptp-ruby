class Election < ApplicationRecord
  extend FriendlyId
  validates :polling_date, presence: true
  validates :country, presence: true

  has_many :constituencies, dependent: :destroy

  has_one :first_past_post_result, dependent: :destroy
  has_one :most_past_post_result, dependent: :destroy

  delegate :url_helpers, to: "Rails.application.routes"
  delegate :year, to: :polling_date, allow_nil: true

  enum election_type: [:general_election]

  friendly_id :country_type_year, use: :slugged
  def country_type_year
    "#{country} #{election_type.to_s.humanize} #{year}"
  end

  def pending?
    polling_date.future? ||
      first_past_post_result.blank? ||
      most_past_post_result.blank?
  end

  def as_json(*)
    {
      "id": slug,
      "polling_date": polling_date,
      "year": year,
      "country": country,
      "election_type": election_type.humanize.downcase,
      "created_at": created_at,
      "updated_at": updated_at,
      "stats": stats,
      "uri": uri(:json),
      "results": {
        "first_past_post_result": first_past_post_result&.as_summary_json,
        "most_past_post_result": most_past_post_result&.as_summary_json
      }
    }
  end

  def uri(format = nil)
    url_helpers.election_path(self, format: format)
  end

  def total_votes
    {
      total: candidates.sum(:votes),
      parties: party_votes,
      electorate: constituencies.sum(:size),
      turnout: (candidates.sum(:votes) / constituencies.sum(:size).to_f) * 100.0
    }
  end

  def party_votes
    keys =
      first_past_post_result
      &.seats
      &.collect { |s| s.candidate.party }
      &.collect { |p| p.slug || "other" }
      &.uniq
    return nil if keys.blank? # TODO: refactor needed

    others = 0
    party_totals = {}
    candidates.group(:party).sum(:votes).each do |entry|
      party = entry[0]
      if keys.include? party.slug
        party_totals[party.slug] = entry[1]
      else
        others += entry[1]
      end
    end
    party_totals
      .sort_by { |_k, v| v }
      .reverse
      .concat([["other", others]])
  end

  def candidates
    Candidate.joins(:constituency).where(constituencies: { election_id: id })
  end

  def parties
    Party.joins(candidates: :constituency)
         .where("constituencies.election_id = ?", id)
         .distinct
         .order(:id)
  end

  def clean
    most_past_post_result&.clean
    most_past_post_result&.destroy
    first_past_post_result&.clean
    first_past_post_result&.destroy

    Candidate.joins(:constituency).where("constituencies.election_id = ?", id).delete_all
    constituencies.delete_all
  end

  def stats
    {
      constituencies: constituencies.size,
      parties: parties.size,
      candidates: candidates.count,
      total_votes: total_votes,
      fpp_results: first_past_post_result&.stats,
      mpp_results: most_past_post_result&.stats
    }
  end

  def calculate_mpp_threshold
    (total_votes[:total] * (Rails.application.config.mpp_threshold_percent / 100.0)).round
  end

  def generate_results
    first_past_post_result = FirstPastPostResult.find_or_create_by(election_id: id)
    first_past_post_result.generate

    MostPastPostResult.find_or_create_by(election_id: id)

    reload
    MPTP::ResultCalculator.new(self).run
    save!
    reload
  end
end
