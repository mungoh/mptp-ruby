class Party < ApplicationRecord
  extend FriendlyId
  has_many :candidates

  validates :name, uniqueness: true, presence: true
  validates :abbreviation, uniqueness: { allow_nil: true }

  NONE = new(
    name: "none",
    abbreviation: "none",
    slug: "none"
  ).freeze

  friendly_id :name, use: :slugged
  def normalize_friendly_id(string)
    string.delete!("'")
    super
  end
end
