class Region < ApplicationRecord
  extend FriendlyId

  has_many :constituencies

  friendly_id :name, use: :slugged

  validates :name, uniqueness: true, presence: true
  validates :slug, uniqueness: true, presence: true
end
