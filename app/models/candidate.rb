class Candidate < ApplicationRecord
  extend FriendlyId

  belongs_to :party
  belongs_to :constituency

  friendly_id :name, use: :scoped, scope: :constituency_id

  validates :name, uniqueness: { scope: :constituency_id }, presence: true
  validates :slug, uniqueness: { scope: :constituency_id }, presence: true

  def as_json(*)
    {
      "slug": slug,
      "name": name,
      "votes": votes
    }
  end
end
