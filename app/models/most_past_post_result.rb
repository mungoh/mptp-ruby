class MostPastPostResult < ElectionResult
  validates :threshold, numericality: { only_integer: true, greater_than: 0, allow_nil: true }

  def clean
    self.threshold = nil
    super
  end

  def as_json(*)
    super.merge(
      "changed_seats": changed_seats.as_json
    )
  end

  def changed_seats
    @changed_seats ||= MPTP::ChangedSeats.new(election.id)
  end

  def mpp_constituencies
    @mpp_constituencies ||= election.first_past_post_result.seats.joins(:candidate).where(candidates: { party_id: mpp_parties }).collect(&:constituency)
  end

  def fpp_seats
    @fpp_seats ||= election.first_past_post_result.seats.joins(:candidate).where.not(candidates: { party_id: mpp_parties })
  end

  def fpp_seats_by_party(party)
    election.first_past_post_result.seats.joins(:candidate).where(candidates: { party_id: party.id })
  end

  def mpp_parties
    unless @mpp_parties
      query = <<-SQL
        WITH election_votes AS (
          SELECT SUM(votes) AS total_votes
          FROM candidates, constituencies
          WHERE candidates.constituency_id = constituencies.id
          AND constituencies.election_id = :election_id
        ),
        party_votes AS (
          SELECT candidates.party_id, SUM(candidates.votes) AS total
          FROM candidates, constituencies, election_votes
          WHERE candidates.constituency_id = constituencies.id
          AND constituencies.election_id = :election_id
          GROUP BY candidates.party_id, election_votes.total_votes
          HAVING SUM(candidates.votes) >= :threshold
        )
        SELECT parties.*, party_votes.total
        FROM parties, party_votes
        WHERE parties.id = party_votes.party_id
      SQL

      @mpp_parties = Party.find_by_sql([query, { election_id: election.id, threshold: threshold }])
    end

    @mpp_parties
  end

  def mpp_party_ids
    mpp_parties.collect(&:id)
  end

  # TODO: refactor - replace with party_rankings
  def mpp_party_constituencies
    @mpp_party_constituencies ||= mpp_parties.collect do |party|
      party
        .attributes
        .merge(ranked_candidates: party_ranked_candidates(party))
        .symbolize_keys
    end
  end

  def party_rankings
    @party_rankings ||= mpp_parties.collect do |party|
      {
        name: party.name,
        abbreviation: party.abbreviation,
        slug: party.slug,
        calculation_stats: party_seat_proportions(party),
        ranked_candidates: party_ranked_candidates(party).collect do |candidate|
          {
            candidate: candidate.as_json,
            constituency: candidate.constituency.as_json
          }
        end
      }
    end
  end

  def party_seat_proportions(party)
    party_proportions.party_seat_proportions.find { |p| p[:party] == party }
  end

  def party_proportions
    @party_proportions ||= MPTP::PartyProportionCalculator.new(election)
  end

  def wasted_votes
    @wasted_votes ||= Stats::WastedMPPVotes.new(election_id, mpp_party_ids).calculate
  end

  private

  # rubocop:disable Metrics/MethodLength
  def party_ranked_candidates(party)
    query = <<-SQL
      WITH election_constituencies AS (
        SELECT constituencies.*
        FROM constituencies
        WHERE constituencies.election_id = #{election_id}
      ),
      mpp_seats AS (
        SELECT seats.*
        FROM candidates, seats, election_results
        WHERE seats.election_result_id = election_results.id
        AND election_results.id = #{id}
        AND seats.candidate_id = candidates.id
        AND candidates.party_id NOT IN (#{mpp_party_ids.join(',')})
      ),
      available_constituencies AS (
        SELECT election_constituencies.*
        FROM election_constituencies
        LEFT JOIN mpp_seats ON (mpp_seats.constituency_id = election_constituencies.id)
        WHERE mpp_seats.id IS NULL
      ),
      ranked_party_candidates AS (
        SELECT candidates.id, candidates.constituency_id, candidates.slug, candidates.votes / available_constituencies.size::NUMERIC AS ratio, rank() over (
          PARTITION BY party_id
          ORDER BY votes / available_constituencies.size::NUMERIC DESC
        ) AS rank
        FROM candidates, available_constituencies
        WHERE candidates.constituency_id = available_constituencies.id
      )
      SELECT constituencies.slug, candidates.* --, ranked_party_candidates.ratio, constituencies.slug, party_id, rank -- id, ranked_candidates.constituency_id
      FROM ranked_party_candidates, candidates, constituencies, parties
      WHERE candidates.id = ranked_party_candidates.id
      AND candidates.constituency_id = constituencies.id
      AND parties.id = candidates.party_id
      AND parties.id = #{party.id}
      ORDER BY rank
    SQL

    Candidate.find_by_sql(query).to_a
  end
  # rubocop:enable Metrics/MethodLength
end
