class FirstPastPostResult < ElectionResult
  def generate
    transaction do
      save! unless persisted?
      seats.destroy_all unless seats.blank?

      query = <<-SQL
        WITH election_constituencies AS (
          SELECT constituencies.*
          FROM elections, constituencies
          WHERE elections.id = #{election_id}
          AND constituencies.election_id = elections.id
        ),
        ranked_candidates AS (
          SELECT candidates.id, candidates.constituency_id, candidates.slug, rank() over (
            PARTITION BY constituency_id
            ORDER BY votes DESC
          ) AS rank
          FROM candidates, election_constituencies
          WHERE candidates.constituency_id = election_constituencies.id
        )
        INSERT INTO seats
        (election_result_id, candidate_id, constituency_id, created_at, updated_at)
        SELECT #{id}, ranked_candidates.id, ranked_candidates.constituency_id, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
        FROM ranked_candidates
        WHERE ranked_candidates.rank = 1
      SQL

      FirstPastPostResult.connection.exec_query(query)

      reload
    end
  end

  def wasted_votes
    @wasted_votes ||= Stats::WastedFPPVotes.new(election_id).calculate
  end
end
