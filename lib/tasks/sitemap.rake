require "builder"

namespace :sitemap do
  desc "Create all sitemaps"
  task :generate, %w[data_dir] => [:environment] do |_t, args|
    data_dir = args[:data_dir].to_s
    raise ArgumentError "no data_dir specified" if data_dir.blank?

    Pathname.new(data_dir)
    puts "generating sitemap in :#{data_dir}"

    domain = "https://mostpastpost.info"
    urls = [
      {
        url: "",
        priority: 0.5
      },
      {
        url: "/how-it-works",
        priority: 0.5
      },
      {
        url: "/how-it-works/voting",
        priority: 0.5
      },
      {
        url: "/how-it-works/counting",
        priority: 0.5
      },
      {
        url: "/how-it-works/threshold",
        priority: 0.5
      },
      {
        url: "/how-it-works/below-threshold-calculation",
        priority: 0.5
      },
      {
        url: "/how-it-works/above-threshold-calculation",
        priority: 0.5
      },
      {
        url: "/demo",
        priority: 0.5
      }
    ]

    Election.all.each do |election|
      if election.pending?
        urls << {
          url: "/pending/#{election.slug}",
          changefreq: "daily",
          priority: 0.5
        }
        next
      end
      urls << {
        url: "/demo/#{election.slug}/dataset",
        priority: 0.5
      }
      urls << {
        url: "/demo/#{election.slug}/calculations",
        priority: 0.5
      }
      urls << {
        url: "/demo/#{election.slug}/results",
        priority: 0.5
      }
    end

    lastmod = Time.now.strftime("%Y-%m-%d")

    xml_output = ""
    xml = Builder::XmlMarkup.new(target: xml_output)
    xml.instruct!
    xml.urlset("xmlns" => "http://www.sitemaps.org/schemas/sitemap/0.9") do
      urls.each do |page|
        xml.url do
          xml.loc "#{domain}#{page[:url]}"
          xml.priority page[:priority].to_s
          xml.changefreq page[:changefreq] if page[:changefreq].present?
          xml.lastmod lastmod
        end
      end
    end

    File.open("#{data_dir}/sitemap.xml", "w") do |file|
      file.write(xml_output.to_s)
    end
  end
end
