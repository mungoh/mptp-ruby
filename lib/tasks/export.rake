namespace :export do
  desc "exports generated elections"
  task :elections, %w[data_dir] => [:environment] do |_t, args|
    data_dir = args[:data_dir].to_s
    data_path =
      if data_dir.blank?
        Rails.root.join("data", "dist")
      else
        Pathname.new(data_dir)
      end
    puts "starting elections export to :#{data_dir}"
    Export::ElectionsExporter.new(dir: data_path).run
  end
end
