namespace :mpp do
  desc "Generate most-past-the-post results"
  task :generate_results, [:year,:threshold] => :environment do |_t, args|
    raise "generate_results task needs valid year argument" unless args.key?(:year)
    raise "generate_results task needs valid threshold argument" unless args.key?(:threshold)

    year = args[:year].to_i
    raise "generate_results task needs valid year argument: #{args[:year]}" unless year.is_a?(Integer) && year >= 1

    election = Election.find_by(year: args[:year])
    raise "election not found for year: #{args[:year]}" unless election.is_a?(Election)

    threshold = args[:threshold].to_i
    raise "generate_results task needs valid threshold argument: #{args[:threshold]}" unless threshold.is_a?(Integer) && threshold >= 1

    generator = GenerateResults.new(election: election, threshold: threshold)
    generator.call
  end
end
