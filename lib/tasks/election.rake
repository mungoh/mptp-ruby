namespace :election do
  desc "loads election data by date, country, type"
  task :load, %i[polling_date country election_type] => [:environment] do |_t, args|
    stats = MPTP::ElectionLoader.new(args.to_h).run

    puts "election loaded successfully:"
    puts stats
  rescue StandardError => e
    puts "election loader failed: #{e.message}"
  end

  desc "loads all GB general elections for which data is available"
  task load_gb_general_elections: [:environment] do
    polling_dates = [
      "1992-04-09",
      "1997-05-01",
      "2001-06-07",
      "2005-05-05",
      "2010-05-06",
      "2015-05-07",
      "2017-06-08"
    ]

    stats = polling_dates.collect do |polling_date|
      MPTP::ElectionLoader.new(
        country: "GB",
        election_type: :general_election,
        polling_date: polling_date
      ).run
    end

    puts "#{stats.count} elections loaded successfully:"
    puts stats
  rescue StandardError => e
    puts "election loader failed: #{e.message}"
    puts e.backtrace
  end
end
