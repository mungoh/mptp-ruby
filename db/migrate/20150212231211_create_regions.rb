class CreateRegions < ActiveRecord::Migration[5.0]
  def change
    create_table :regions do |t|
      t.string :name, null: false
      t.string :slug, null: false

      t.timestamps null: false
    end
    add_index :regions, :name, unique: true
    add_index :regions, :slug, unique: true
  end
end
