class CreateParties < ActiveRecord::Migration[5.0]
  def change
    create_table :parties do |t|
      t.string :name, null: false
      t.string :slug, null: false
      t.string :short_name
      t.string :abbreviation
      t.string :colour

      t.timestamps null: false
    end
  end
end
