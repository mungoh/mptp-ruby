class CreateElectionResults < ActiveRecord::Migration[5.0]
  def change
    create_table :election_results do |t|
      t.belongs_to :election, foreign_key: true
      t.string     :type, null: false
      t.integer    :threshold, null: true

      t.timestamps null: false
    end

    add_index(:election_results, [:election_id, :type, :threshold], unique: true)
  end
end
