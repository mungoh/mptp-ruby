class CreateCandidates < ActiveRecord::Migration[5.0]
  def change
    create_table :candidates do |t|
      t.belongs_to :party, index: true, foreign_key: true
      t.belongs_to :constituency, index: true, foreign_key: true

      t.string     :name, null: false
      t.string     :slug, null: false
      t.integer    :votes

      t.timestamps
    end

    add_index(:candidates, [:constituency_id, :slug], unique: true)
    add_index(:candidates, [:constituency_id, :votes])
  end
end
