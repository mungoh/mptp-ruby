class CreateResultCalculations < ActiveRecord::Migration[5.1]
  def change
    create_table :result_calculations do |t|
      t.belongs_to :election_result, index: true, foreign_key: true
      t.integer    :order_seq
      t.string     :description
      t.integer    :calculation_type, null: false
      t.timestamps null: false
    end

    create_table :seat_calculations do |t|
      t.belongs_to :result_calculation, index: true, foreign_key: true
      t.belongs_to :seat, index: true, foreign_key: true
      t.timestamps null: false
    end

    add_index :result_calculations, [:election_result_id, :order_seq], unique: true
    add_index :seat_calculations, [:result_calculation_id, :seat_id], unique: true
  end
end
