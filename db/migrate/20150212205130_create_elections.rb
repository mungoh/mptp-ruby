class CreateElections < ActiveRecord::Migration[5.0]
  def change
    create_table :elections do |t|
      t.date :polling_date, null: false
      t.string  :slug, null: false
      t.string :country, limit: 2, null: false
      t.integer :election_type, default: 0, null: false
      t.timestamps
    end

    add_reference :elections, :first_past_post_result, references: :election_results
    add_reference :elections, :most_past_post_result, references: :election_results
    add_index(:elections, :slug, unique: true)
    add_index(:elections, [:polling_date, :country, :election_type], unique: true)
  end
end
