class CreateConstituencies < ActiveRecord::Migration[5.0]
  def change
    create_table :constituencies do |t|
      t.belongs_to :election,       index: true, foreign_key: true
      t.belongs_to :region,         index: true, foreign_key: true
      t.string     :name,           null: false
      t.string     :slug,           null: false
      t.string     :abbreviation
      t.integer    :size,           index: true, null: true
      t.string     :ons_id
      t.timestamps
    end

    add_index(:constituencies, [:election_id, :slug, :name], unique: true)
    add_index(:constituencies, [:election_id, :ons_id], unique: true)
  end
end
