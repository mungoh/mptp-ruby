class CreateSeats < ActiveRecord::Migration[5.0]
  def change
    create_table :seats do |t|
      t.belongs_to :election_result, index: true, foreign_key: true
      t.belongs_to :candidate,       index: true, foreign_key: true
      t.belongs_to :constituency,    index: true, foreign_key: true
      t.json       :metadata
      t.timestamps null: false
    end
  end
end
