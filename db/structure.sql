SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: candidates; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.candidates (
    id integer NOT NULL,
    party_id integer,
    constituency_id integer,
    name character varying NOT NULL,
    slug character varying NOT NULL,
    votes integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: candidates_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.candidates_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: candidates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.candidates_id_seq OWNED BY public.candidates.id;


--
-- Name: constituencies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.constituencies (
    id integer NOT NULL,
    election_id integer,
    region_id integer,
    name character varying NOT NULL,
    slug character varying NOT NULL,
    abbreviation character varying,
    size integer,
    ons_id character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: constituencies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.constituencies_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: constituencies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.constituencies_id_seq OWNED BY public.constituencies.id;


--
-- Name: election_results; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.election_results (
    id integer NOT NULL,
    election_id integer,
    type character varying NOT NULL,
    threshold integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: election_results_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.election_results_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: election_results_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.election_results_id_seq OWNED BY public.election_results.id;


--
-- Name: elections; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.elections (
    id integer NOT NULL,
    polling_date date NOT NULL,
    slug character varying NOT NULL,
    country character varying(2) NOT NULL,
    election_type integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    first_past_post_result_id integer,
    most_past_post_result_id integer
);


--
-- Name: elections_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.elections_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: elections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.elections_id_seq OWNED BY public.elections.id;


--
-- Name: friendly_id_slugs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.friendly_id_slugs (
    id integer NOT NULL,
    slug character varying NOT NULL,
    sluggable_id integer NOT NULL,
    sluggable_type character varying(50),
    scope character varying,
    created_at timestamp without time zone
);


--
-- Name: friendly_id_slugs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.friendly_id_slugs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: friendly_id_slugs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.friendly_id_slugs_id_seq OWNED BY public.friendly_id_slugs.id;


--
-- Name: parties; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.parties (
    id integer NOT NULL,
    name character varying NOT NULL,
    slug character varying NOT NULL,
    short_name character varying,
    abbreviation character varying,
    colour character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: parties_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.parties_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: parties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.parties_id_seq OWNED BY public.parties.id;


--
-- Name: regions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.regions (
    id integer NOT NULL,
    name character varying NOT NULL,
    slug character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: regions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.regions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: regions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.regions_id_seq OWNED BY public.regions.id;


--
-- Name: result_calculations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.result_calculations (
    id bigint NOT NULL,
    election_result_id bigint,
    order_seq integer,
    description character varying,
    calculation_type integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: result_calculations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.result_calculations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: result_calculations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.result_calculations_id_seq OWNED BY public.result_calculations.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: seat_calculations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.seat_calculations (
    id bigint NOT NULL,
    result_calculation_id bigint,
    seat_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: seat_calculations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.seat_calculations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: seat_calculations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.seat_calculations_id_seq OWNED BY public.seat_calculations.id;


--
-- Name: seats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.seats (
    id integer NOT NULL,
    election_result_id integer,
    candidate_id integer,
    constituency_id integer,
    metadata json,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: seats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.seats_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: seats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.seats_id_seq OWNED BY public.seats.id;


--
-- Name: candidates id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.candidates ALTER COLUMN id SET DEFAULT nextval('public.candidates_id_seq'::regclass);


--
-- Name: constituencies id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.constituencies ALTER COLUMN id SET DEFAULT nextval('public.constituencies_id_seq'::regclass);


--
-- Name: election_results id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.election_results ALTER COLUMN id SET DEFAULT nextval('public.election_results_id_seq'::regclass);


--
-- Name: elections id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.elections ALTER COLUMN id SET DEFAULT nextval('public.elections_id_seq'::regclass);


--
-- Name: friendly_id_slugs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.friendly_id_slugs ALTER COLUMN id SET DEFAULT nextval('public.friendly_id_slugs_id_seq'::regclass);


--
-- Name: parties id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.parties ALTER COLUMN id SET DEFAULT nextval('public.parties_id_seq'::regclass);


--
-- Name: regions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.regions ALTER COLUMN id SET DEFAULT nextval('public.regions_id_seq'::regclass);


--
-- Name: result_calculations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.result_calculations ALTER COLUMN id SET DEFAULT nextval('public.result_calculations_id_seq'::regclass);


--
-- Name: seat_calculations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seat_calculations ALTER COLUMN id SET DEFAULT nextval('public.seat_calculations_id_seq'::regclass);


--
-- Name: seats id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seats ALTER COLUMN id SET DEFAULT nextval('public.seats_id_seq'::regclass);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: candidates candidates_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.candidates
    ADD CONSTRAINT candidates_pkey PRIMARY KEY (id);


--
-- Name: constituencies constituencies_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.constituencies
    ADD CONSTRAINT constituencies_pkey PRIMARY KEY (id);


--
-- Name: election_results election_results_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.election_results
    ADD CONSTRAINT election_results_pkey PRIMARY KEY (id);


--
-- Name: elections elections_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.elections
    ADD CONSTRAINT elections_pkey PRIMARY KEY (id);


--
-- Name: friendly_id_slugs friendly_id_slugs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.friendly_id_slugs
    ADD CONSTRAINT friendly_id_slugs_pkey PRIMARY KEY (id);


--
-- Name: parties parties_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.parties
    ADD CONSTRAINT parties_pkey PRIMARY KEY (id);


--
-- Name: regions regions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.regions
    ADD CONSTRAINT regions_pkey PRIMARY KEY (id);


--
-- Name: result_calculations result_calculations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.result_calculations
    ADD CONSTRAINT result_calculations_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: seat_calculations seat_calculations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seat_calculations
    ADD CONSTRAINT seat_calculations_pkey PRIMARY KEY (id);


--
-- Name: seats seats_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seats
    ADD CONSTRAINT seats_pkey PRIMARY KEY (id);


--
-- Name: index_candidates_on_constituency_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_candidates_on_constituency_id ON public.candidates USING btree (constituency_id);


--
-- Name: index_candidates_on_constituency_id_and_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_candidates_on_constituency_id_and_slug ON public.candidates USING btree (constituency_id, slug);


--
-- Name: index_candidates_on_constituency_id_and_votes; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_candidates_on_constituency_id_and_votes ON public.candidates USING btree (constituency_id, votes);


--
-- Name: index_candidates_on_party_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_candidates_on_party_id ON public.candidates USING btree (party_id);


--
-- Name: index_constituencies_on_election_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_constituencies_on_election_id ON public.constituencies USING btree (election_id);


--
-- Name: index_constituencies_on_election_id_and_ons_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_constituencies_on_election_id_and_ons_id ON public.constituencies USING btree (election_id, ons_id);


--
-- Name: index_constituencies_on_election_id_and_slug_and_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_constituencies_on_election_id_and_slug_and_name ON public.constituencies USING btree (election_id, slug, name);


--
-- Name: index_constituencies_on_region_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_constituencies_on_region_id ON public.constituencies USING btree (region_id);


--
-- Name: index_constituencies_on_size; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_constituencies_on_size ON public.constituencies USING btree (size);


--
-- Name: index_election_results_on_election_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_election_results_on_election_id ON public.election_results USING btree (election_id);


--
-- Name: index_election_results_on_election_id_and_type_and_threshold; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_election_results_on_election_id_and_type_and_threshold ON public.election_results USING btree (election_id, type, threshold);


--
-- Name: index_elections_on_first_past_post_result_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_elections_on_first_past_post_result_id ON public.elections USING btree (first_past_post_result_id);


--
-- Name: index_elections_on_most_past_post_result_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_elections_on_most_past_post_result_id ON public.elections USING btree (most_past_post_result_id);


--
-- Name: index_elections_on_polling_date_and_country_and_election_type; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_elections_on_polling_date_and_country_and_election_type ON public.elections USING btree (polling_date, country, election_type);


--
-- Name: index_elections_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_elections_on_slug ON public.elections USING btree (slug);


--
-- Name: index_friendly_id_slugs_on_slug_and_sluggable_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_friendly_id_slugs_on_slug_and_sluggable_type ON public.friendly_id_slugs USING btree (slug, sluggable_type);


--
-- Name: index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope ON public.friendly_id_slugs USING btree (slug, sluggable_type, scope);


--
-- Name: index_friendly_id_slugs_on_sluggable_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_friendly_id_slugs_on_sluggable_id ON public.friendly_id_slugs USING btree (sluggable_id);


--
-- Name: index_friendly_id_slugs_on_sluggable_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_friendly_id_slugs_on_sluggable_type ON public.friendly_id_slugs USING btree (sluggable_type);


--
-- Name: index_regions_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_regions_on_name ON public.regions USING btree (name);


--
-- Name: index_regions_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_regions_on_slug ON public.regions USING btree (slug);


--
-- Name: index_result_calculations_on_election_result_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_result_calculations_on_election_result_id ON public.result_calculations USING btree (election_result_id);


--
-- Name: index_result_calculations_on_election_result_id_and_order_seq; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_result_calculations_on_election_result_id_and_order_seq ON public.result_calculations USING btree (election_result_id, order_seq);


--
-- Name: index_seat_calculations_on_result_calculation_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_seat_calculations_on_result_calculation_id ON public.seat_calculations USING btree (result_calculation_id);


--
-- Name: index_seat_calculations_on_result_calculation_id_and_seat_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_seat_calculations_on_result_calculation_id_and_seat_id ON public.seat_calculations USING btree (result_calculation_id, seat_id);


--
-- Name: index_seat_calculations_on_seat_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_seat_calculations_on_seat_id ON public.seat_calculations USING btree (seat_id);


--
-- Name: index_seats_on_candidate_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_seats_on_candidate_id ON public.seats USING btree (candidate_id);


--
-- Name: index_seats_on_constituency_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_seats_on_constituency_id ON public.seats USING btree (constituency_id);


--
-- Name: index_seats_on_election_result_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_seats_on_election_result_id ON public.seats USING btree (election_result_id);


--
-- Name: seat_calculations fk_rails_07578c50c3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seat_calculations
    ADD CONSTRAINT fk_rails_07578c50c3 FOREIGN KEY (seat_id) REFERENCES public.seats(id);


--
-- Name: seats fk_rails_17981fc181; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seats
    ADD CONSTRAINT fk_rails_17981fc181 FOREIGN KEY (election_result_id) REFERENCES public.election_results(id);


--
-- Name: election_results fk_rails_1c12e28a90; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.election_results
    ADD CONSTRAINT fk_rails_1c12e28a90 FOREIGN KEY (election_id) REFERENCES public.elections(id);


--
-- Name: constituencies fk_rails_4cdd5d892a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.constituencies
    ADD CONSTRAINT fk_rails_4cdd5d892a FOREIGN KEY (election_id) REFERENCES public.elections(id);


--
-- Name: constituencies fk_rails_5b422d1b1f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.constituencies
    ADD CONSTRAINT fk_rails_5b422d1b1f FOREIGN KEY (region_id) REFERENCES public.regions(id);


--
-- Name: seats fk_rails_6d2e79ff23; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seats
    ADD CONSTRAINT fk_rails_6d2e79ff23 FOREIGN KEY (constituency_id) REFERENCES public.constituencies(id);


--
-- Name: candidates fk_rails_8afef8e095; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.candidates
    ADD CONSTRAINT fk_rails_8afef8e095 FOREIGN KEY (constituency_id) REFERENCES public.constituencies(id);


--
-- Name: seats fk_rails_98fa79f22b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seats
    ADD CONSTRAINT fk_rails_98fa79f22b FOREIGN KEY (candidate_id) REFERENCES public.candidates(id);


--
-- Name: result_calculations fk_rails_a3745b189b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.result_calculations
    ADD CONSTRAINT fk_rails_a3745b189b FOREIGN KEY (election_result_id) REFERENCES public.election_results(id);


--
-- Name: candidates fk_rails_e0db8e5867; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.candidates
    ADD CONSTRAINT fk_rails_e0db8e5867 FOREIGN KEY (party_id) REFERENCES public.parties(id);


--
-- Name: seat_calculations fk_rails_ede89f78dd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seat_calculations
    ADD CONSTRAINT fk_rails_ede89f78dd FOREIGN KEY (result_calculation_id) REFERENCES public.result_calculations(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20150212205130'),
('20150212231211'),
('20150212231212'),
('20150212231213'),
('20150212231343'),
('20150215121230'),
('20150215121302'),
('20161103140828'),
('20180120143750');


