after :elections do
  Election.all.each do |election|
    puts ""
    puts "processing election results: #{election.year}"

    # add first past the post seats to new fpp election result
    fpp_result = election.first_past_post_result
    unless fpp_result.is_a?(ElectionResult)
      fpp_result = FirstPastPostResult.find_or_create_by(election: election)

      seats = []
      election.constituencies.each do |constituency|
        fpp_winner = Candidate.where{constituency_id == constituency.id}.order{votes.desc}.first
        unless fpp_winner.is_a?(Candidate)
          Rails.logger.warn "no winner found for constituency: #{constituency.name}"
          print "x"
          next
        end

        seats << Seat.find_or_create_by(
          candidate_id: fpp_winner.id,
          election_result_id: fpp_result.id,
          constituency_id: constituency.id
        )
        print "."
      end
      fpp_result.seats = seats

      election.first_past_post_result = fpp_result
    end
  end
end
