require 'csv'

after :constituencies do
  Election.all.each do |election|
    puts ""
    puts "creating candidates for election: #{election.year}"
    case election.year
    when 2015
      file = "#{Rails.root}/db/import/#{election.year}_election_results.json"
      results = JSON.parse(File.read(file)).with_indifferent_access
      results[:constituencies].each do |c|
        constituency = Constituency.find_by! slug: c[:name].slugify, election: election

        c[election.year.to_s][:candidates].each do |candidacy|
          if candidacy[:party]
            party = Party.find_or_create_by(abbreviation: candidacy[:party])
          end

          Candidate.find_or_create_by(
            constituency: constituency,
            party:        party,
            name:         candidacy[:name],
            slug:         candidacy[:name].slugify,
            votes:        candidacy[:votes]
          )
          print "."
        end
      end
    else
      Dir["#{Rails.root}/db/import/#{election.year}_candidate_*.json"].each do |file|
        candidate = JSON.parse(File.read(file)).with_indifferent_access

        candidate["person"]["candidacies"].each do |candidacy|
          next unless candidacy["election"]["type"] == "general election" && candidacy["election"]["year"].to_i == election.year

          constituency = Constituency.find_by! slug: candidacy["constituency"]["name"].slugify, election: election

          if candidacy["party"]
            party = Party.find_or_create_by(name: candidacy["party"]["name"], abbreviation: candidacy["party"]["abbreviation"])
          end

          Candidate.find_or_create_by(
            constituency: constituency,
            party:        party,
            name:         candidate["person"]["name"],
            slug:         candidate["person"]["name"].slugify,
            votes:        candidacy["votes-as-quantity"]
          )
          print "."
        end
      end

      Dir["#{Rails.root}/db/import/exceptions_#{election.year}_*.json"].each do |file|
        constituency_name = file.sub(/.*exceptions_#{election.year}_/, "").sub(/\.json$/, "")
        constituency = Constituency.find_by(election: election, name: constituency_name)
        puts ""
        puts "processing exception for #{election.year}: #{constituency_name}"

        root = JSON.parse(File.read(file)).with_indifferent_access
        root[:exceptions].each do |exception|
          next unless exception[:year] == election.year

          exception[:candidates].each do |candidacy|
            if candidacy["party"]
              party = Party.find_or_create_by(name: candidacy["party"]["name"])
            end

            Candidate.find_or_create_by(
              constituency: constituency,
              party:        party,
              name:         candidacy["name"],
              slug:         candidacy["name"].slugify,
              votes:        candidacy["votes"]
            )

            constituency.size = exception[:electorate]
            constituency.save!

            print "."
          end
        end
      end
    end
    puts ""
  end
end
