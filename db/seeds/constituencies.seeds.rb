require 'csv'

after :elections, :regions do
  Election.all.each do |election|
    puts ""
    puts "creating constituencies for election: #{election.year}"
    case election.year
    when 2015
      file = "#{Rails.root}/db/import/#{election.year}_election_results.json"
      results = JSON.parse(File.read(file)).with_indifferent_access
      results[:constituencies].each do |constituency|
        c_record = Constituency.find_or_create_by(
            election: election,
            name:     constituency[:name],
            slug:     constituency[:name].slugify
          )
        c_record.size = constituency[election.year.to_s][:electorate]
        election.constituencies << c_record
        print "."

      end
    else
      Dir["#{Rails.root}/db/import/#{election.year}_constituency_*.json"].each do |file|
        constituency = JSON.parse(File.read(file)).with_indifferent_access

        size = 0
        constituency[:constituency][:contests].each do |contest|
          next unless contest[:type] == "general election" && contest[:year].to_i == election.year

          size = ((contest["turnout-as-quantity"] / contest["turnout-as-percentage"]) * 100).round
          break
        end

        Rails.logger.warn("size not found for constituency: #{constituency[:constituency][:name]}")

        c_record = Constituency.find_or_create_by(
            election: election,
            name:     constituency[:constituency][:name],
            slug:     constituency[:constituency][:name].slugify
          )
        c_record.size = size
        election.constituencies << c_record
        print "."
      end
      puts ""
    end
  end
end
